<?php

/**
 * @version    CVS: 1.0.0
 * @package    com_alkasubscriptions
 * @author     Alka <developpement@alkaonline.be>
 * @copyright  2017 Alka
 * @license    GNU General Public License version 2 ou version ultérieure ; Voir LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Class AlkaSubscriptionsController
 *
 * @since  1.0
 */
class AlkaSubscriptionsControllerSubscriptions extends JControllerLegacy
{
    public function trashSub(){
      $user = JFactory::getUser();
        $subId = JFactory::getApplication()->input->get('idSub',null,null);
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $now = new DateTime();
        $query->update($db->quoteName('#__alkasubscriptions_subscription'))->set(
          array(
            'trashed = TRUE',
            'enabled = -1',
            'sendNotification = 0',
            'modifiedBy = '.$user->id,
            'modifiedDate = '.$db->quote($now->format('Y-m-d H:i:s'))
          )
        )->where(array($db->quoteName('id') . ' = ' . $subId));

        $db->setQuery($query);

        $db->execute();
        echo new JResponseJson(true);
        die();
      }
    public function setState(){
        $subscription = (object) JFactory::getApplication()->input->get('subscriptions',null,null);

        $id = JFactory::getApplication()->input->get('id', null);
        $state = (int)JFactory::getApplication()->input->get('state', null);

        $currentUserID = JFactory::getUser()->id;
        $nowDate = new DateTime();

        $enabled = null;

        // Get a db connection
        $db = JFactory::getDbo();

        // Create a new query object
        $query = $db->getQuery(true);

        if($state == 1){
          $enabled = 0;
        } else if($state == 0){
          $enabled = 1;
        }

        if($id){
          // Update query

          // Fields to update
          $fields = array(
              $db->quoteName('enabled') . ' = ' . $enabled,
              $db->quoteName('modifiedDate') . ' = ' . $db->quote($nowDate->format('Y-m-d H:i:s')),
              $db->quoteName('modifiedBy') . ' = ' . $currentUserID
          );

          // Conditions for which records should be updated
          $conditions = array(
              $db->quoteName('id') . ' = ' . $id
          );

          $query->update($db->quoteName('#__alkasubscriptions_subscription'))->set($fields)->where($conditions);

          $db->setQuery($query);

          $result = $db->execute();

          if($result){
            JFactory::getApplication()->redirect('administrator/index.php?option=com_alkasubscriptions');
            return $id;
          } else {
            echo '<div class="alert alert-warning">Update failed.</div>';
            return false;
          }
      }
    }

    public function setNotif(){
        $subscription = (object) JFactory::getApplication()->input->get('subscriptions',null,null);

        $id = JFactory::getApplication()->input->get('id', null);
        $state = (int)JFactory::getApplication()->input->get('state', null);

        $currentUserID = JFactory::getUser()->id;
        $nowDate = new DateTime();

        $enabled = null;

        // Get a db connection
        $db = JFactory::getDbo();

        // Create a new query object
        $query = $db->getQuery(true);

        if($state == 1){
          $notif = 0;
        } else if($state == 0){
          $notif = 1;
        }

        if($id){
          // Update query

          // Fields to update
          $fields = array(
              $db->quoteName('sendNotification') . ' = ' . $notif,
              $db->quoteName('modifiedDate') . ' = ' . $db->quote($nowDate->format('Y-m-d H:i:s')),
              $db->quoteName('modifiedBy') . ' = ' . $currentUserID
          );

          // Conditions for which records should be updated
          $conditions = array(
              $db->quoteName('id') . ' = ' . $id
          );

          $query->update($db->quoteName('#__alkasubscriptions_subscription'))->set($fields)->where($conditions);

          $db->setQuery($query);

          $result = $db->execute();

          if($result){
            JFactory::getApplication()->redirect('administrator/index.php?option=com_alkasubscriptions');
            return $id;
          } else {
            echo '<div class="alert alert-warning">Update failed.</div>';
            return false;
          }
      }
    }

    public function linkAddress(){
  		$idAddress = JFactory::getApplication()->input->get('idAddress', null);
  		$idSubscription = JFactory::getApplication()->input->get('idSubscription', null);

  		$db = JFactory::getDbo();
  		$query = $db->getQuery(true);

  		$fields = array(
  			$db->quoteName('idAddress') . ' = ' . $idAddress
  		);

  		$conditions = array(
  				$db->quoteName('id') . ' = '. $idSubscription
  		);

  		$query->update($db->quoteName('#__alkasubscriptions_subscription'))->set($fields)->where($conditions);

  		$db->setQuery($query);

  		$result = $db->execute();

  		echo new JResponseJson(true, $message);
  		die();
  	}

    public function setDate(){
        $data = (object)JFactory::getApplication()->input->get('subscription', null, 'ARRAY');

        $startDate = new DateTime();
        $startDate = $startDate->createFromFormat("d-m-Y", $data->startDate);

        $endDate = new DateTime();
        $endDate = $endDate->createFromFormat("d-m-Y",$data->endDate);

        $now = new DateTime();
        $user = JFactory::getUser();

        $data->startDate = $startDate->format('Y-m-d');
        $data->endDate = $endDate->format('Y-m-d');
        $data->modifiedDate = $now->format('Y-m-d H:i:s');
        $data->modifiedBy = $user->id;
        $result = JFactory::getDbo()->updateObject('#__alkasubscriptions_subscription', $data, 'id');
        $data->hStartDate = $startDate->format('d/m/Y');
        $data->hEndDate = $endDate->format('d/m/Y');
        echo new JResponseJson($result,$data);

  		die();
    }

    public function setIssue(){
      $sent = (object)JFactory::getApplication()->input->get('subscription_issues', null, 'ARRAY');

      $db = JFactory::getDbo();
			$query = $db->getQuery(true);

			$query->select('#__alkasubscriptions_subscription.*');
			$query->from($db->quoteName('#__alkasubscriptions_subscription'));
      $query->where($db->quoteName('id') . ' = '. $db->quote($sent->id));

			$db->setQuery($query);
			$subscription = $db->loadObject();
      if(intval($sent->startIssue) < intval($sent->endIssue)){
        $startIssue = $sent->startIssue;
        $endIssue   = $sent->endIssue;

        $now                    = new DateTime();
        $user                   = JFactory::getUser();
        $data                   = new StdClass();
        $data->id               = $sent->id;
        $data->startIssueNumber = $startIssue;
        $data->endIssueNumber   = $endIssue;
        $data->modifiedDate     = $now->format('Y-m-d H:i:s');
        $data->modifiedBy       = $user->id;
        $result                 = JFactory::getDbo()->updateObject('#__alkasubscriptions_subscription', $data, 'id');

        $db = JFactory::getDbo();
  			$query = $db->getQuery(true);

  			$query->select('#__alkasubscriptions_renew_history.*');
  			$query->from($db->quoteName('#__alkasubscriptions_renew_history'));
        $query->where($db->quoteName('subscription_id') . ' = '. $db->quote($sent->id));
        // $query->where($db->quoteName('startIssue') . ' = '. $db->quote($subscription->startIssueNumber));
        // $query->where($db->quoteName('endIssue') . ' = '. $db->quote($subscription->endIssueNumber));

  			$db->setQuery($query);
  			$renew = $db->loadObject();
        if($renew){
          $renew->start = AlkaSubscriptionsHelper::getDeadlineDateByIssue($startIssue, $renew->productType);
          $renew->end = AlkaSubscriptionsHelper::getDeadlineDateByIssue($endIssue, $renew->productType);
          $renew->startIssue = $startIssue;
          $renew->endIssue = $endIssue;
          JFactory::getDbo()->updateObject('#__alkasubscriptions_renew_history', $renew, 'id');
        }

        echo new JResponseJson($result,$data);
      }else{
        throw new Exception('Start issue is higher than end issue', 403);
      }

  		die();
    }
}
