<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * HelloWorldList Model
 *
 * @since  0.0.1
 */
class AlkaSubscriptionsModelSubscriptions extends JModelList
{

    /**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'idSub',
                'productCode',
                'productOptions',
                'username',
                'firstname',
                'name',
                'paid',
                'notification'
			);
		}

		parent::__construct($config);
	}




	/**
	 * Method to build an SQL query to load the list data.
	 *
	 * @return      string  An SQL query
	 */
	protected function getListQuery()
	{
        $db     = JFactory::getDBO();
        $query  = $db->getQuery(true);

        $query->select(
                array(
                        $db->quoteName('s.id','idSub'),
                        $db->quoteName('r.productName','name'),
                        $db->quoteName('s.name','initialName'),
                        $db->quoteName('r.productId','idProduct'),
                        $db->quoteName('s.idProduct','initialIdProduct'),
                        $db->quoteName('r.code','productCode'),
                        $db->quoteName('p.product_code','initialProductCode'),
                        $db->quoteName('s.productOptions'),
                        $db->quoteName('s.startDate'),
                        $db->quoteName('s.endDate'),
                        //$db->quoteName('s.idOrder'),
                        $db->quoteName('s.startIssueNumber'),
                        $db->quoteName('s.endIssueNumber'),
                        $db->quoteName('s.name'),
                        $db->quoteName('s.paid'),
                        $db->quoteName('s.idUser'),
                        $db->quoteName('u.username'),
                        $db->quoteName('u.email'),
                        // $db->quoteName('ha.address_telephone'),
                        //$db->quoteName('(SELECT  FROM )','address_telephone'),
                        $db->quoteName('a.firstname'),
                        $db->quoteName('a.lastname'),
                        $db->quoteName('a.company'),
                        'GROUP_CONCAT('.$db->quoteName('g.title').' SEPARATOR \';\') AS groups',
                        $db->quoteName('a.addressLine1'),
                        $db->quoteName('a.addressLine2'),
                        $db->quoteName('a.zip'),
                        $db->quoteName('a.city'),
                        $db->quoteName('a.country'),
                        //$db->quoteName('s.forever'),
                        $db->quoteName('s.enabled'),
                        $db->quoteName('s.sendNotification'),
                        $db->quoteName('a.id','idAdd'),

                )
        );

        $query->from($db->quoteName('#__alkasubscriptions_subscription', 's'));
        // $query->join('LEFT', $db->quoteName('#__hikashop_order', 'o')
        // . ' ON (o.order_alkasubscription_resubscription_options LIKE CONCAT (\'%"\',s.id,\'"%\'))');
        $query->join('LEFT', $db->quoteName('#__alkasubscriptions_renew_history', 'r')
        . ' ON (r.subscription_id = s.id AND r.start <= NOW() AND r.end > NOW())');
        // $query->join('LEFT', $db->quoteName('#__hikashop_address', 'ha')
        // . ' ON (ha.address_id = o.order_billing_address_id)');
        $query->group('s.id');
        /*$query->join('LEFT', $db->quoteName('#__hikashop_order', 'o')
        . ' ON (' .
        $db->quoteName('o.order_id')
        . ' = ' .
        $db->quoteName('s.idOrder') . ')');

        $query->join('LEFT', $db->quoteName('#__hikashop_address', 'ba')
        . ' ON (' .
        $db->quoteName('ba.address_id')
        . ' = ' .
        $db->quoteName('o.order_billing_address_id') . ')');*/

        $query->join('LEFT', $db->quoteName('#__alkasubscriptions_address', 'a')
        . ' ON (' .
        $db->quoteName('a.id')
        . ' = ' .
        $db->quoteName('s.idAddress') . ')');

        $query->join('LEFT', $db->quoteName('#__users', 'u')
        . ' ON (' .
        $db->quoteName('s.idUser')
        . ' = ' .
        $db->quoteName('u.id') . ')');

        $query->join('LEFT', $db->quoteName('#__hikashop_product', 'p')
        . ' ON (' .
        $db->quoteName('s.idProduct')
        . ' = ' .
        $db->quoteName('p.product_id') . ')');

        $query->join('LEFT', $db->quoteName('#__user_usergroup_map', 'gm')
        . ' ON (' .
        $db->quoteName('gm.user_id')
        . ' = ' .
        $db->quoteName('u.id') . ')');

        $query->join('LEFT', $db->quoteName('#__usergroups', 'g')
        . ' ON (' .
        $db->quoteName('g.id')
        . ' = ' .
        $db->quoteName('gm.group_id') . ')');
        $query->group($db->quoteName('s.id'));
				$query->where('(s.trashed IS NULL OR s.trashed = false)');

        // Filter: like / search
		$search = $this->getState('filter.search');

		if (!empty($search))
		{
			$like = $db->quote('%' . $search . '%');
			$query->where('(
                s.id LIKE ' . $like.'
                OR s.name LIKE ' . $like.'
                OR p.product_code LIKE ' . $like.'
                OR s.productOptions LIKE ' . $like.'
                OR username LIKE ' . $like.'
                OR firstname LIKE ' . $like.'
                OR lastname LIKE ' . $like.'
                OR company LIKE ' . $like.'
                OR addressLine1 LIKE ' . $like.'
                OR addressLine2 LIKE ' . $like.'
                )');
		}

        // Filter by notification state
		$notification = $this->getState('filter.notification');

		if (is_numeric($notification))
		{
			$query->where('s.sendNotification = ' . (int) $notification);
		}
		elseif ($notification === '')
		{
			$query->where('(s.sendNotification IN (0, 1))');
		}

        // Filter by paid state
		$paid = $this->getState('filter.paid');

		if (is_numeric($paid))
		{
			$query->where('s.paid = ' . (int) $paid);
		}
		elseif ($paid === '')
		{
			$query->where('(s.paid IN (0, 1))');
		}

        // Filter by published state
		$published = $this->getState('filter.published');

		if (is_numeric($published))
		{
			$query->where('s.enabled = ' . (int) $published);
		}
		elseif ($published === '')
		{
			$query->where('(s.enabled IN (0, 1))');
		}

        // Add the list ordering clause.
		$orderCol	= $this->state->get('list.ordering', 'idSub');
		$orderDirn 	= $this->state->get('list.direction', 'desc');

		$query->order($db->escape($orderCol) . ' ' . $db->escape($orderDirn));
        if(JFactory::getApplication()->input->get('task')=="subscriptions.exports"){
            $db = JFactory::getDbo();
            $db->setQuery($query);
            $results = $db->loadObjectList();
            foreach($results as &$result){
                $groups =  explode(';',$result->groups);
                $result->groups = implode(",",$groups);
                $type = @unserialize($result->productOptions);
                if(isset($type[0]->characteristic_value)){$result->productOptions= $type[0]->characteristic_value;}else{$result->productOptions= '-';}
            }
            $now = new DateTime();
            $filename = $now->format('d-m-Y_H-i-s').'_'.substr(md5(uniqid()),0,5).".csv";
            $fp = fopen(JPATH_COMPONENT_ADMINISTRATOR.'/exports/'.$filename, 'w');
            $columns = array(
                    'ID',
                    'NAME',
                    'IDPRODUCT',
                    'PRODUCTCODE',
                    'PRODUCTOPTIONS',
                    'STARTDATE',
                    'ENDDATE',
                    //'IDORDER',
                    'STARTISSUENUMBER',
                    'ENDISSUENUMBER',
                    'PAID',
                    'IDUSER',
                    'USERNAME',
                    'EMAIL',
                    'BILLING_ADDRESS_TELEPHONE',
                    'FIRSTNAME',
                    'LASTNAME',
                    'COMPANY',
                    'GROUPS',
                    'STREET',
                    'NUMBER',
                    'ZIP',
                    'CITY',
                    'COUNTRY',
                    'ENABLED',
                    'SENDNOTIFICATION',
                    'IDADDRESS'
            );
            fputcsv($fp, $columns);
            foreach ($results as $fields) {
              if(!$fields->name){
                $fields->name = $fields->initialName;
                $fields->productCode = $fields->initialProductCode;
              }
                if( is_object($fields) )
                $fields = (array) $fields;
                $fields['productCode'] = $fields['productCode'] ? $fields['productCode'] : $fields['initialProductCode'];
                $fields['idProduct'] = $fields['idProduct'] ? $fields['idProduct'] : $fields['initialIdProduct'];
                $fields['productName'] = $fields['productName'] ? $fields['productName'] : $fields['initialName'];
                unset($fields['initialName']);
                unset($fields['initialIdProduct']);
                unset($fields['initialProductCode']);
                fputcsv($fp, $fields);
            }
            fclose($fp);
            JFactory::getApplication()->enqueueMessage('Export successfull, the download should start automatically. <span id="downloadContainer">If not please click here : <a id="downloadExport" href="/components/com_alkasubscriptions/exports/'.$filename.'" >Download result</a></span>', 'success');
            $document = JFactory::getDocument();
            $document->addScriptDeclaration('jQuery(document).ready(function(){
                 var link=document.createElement("a");
                 document.body.appendChild(link);
                 link.href="/administrator/components/com_alkasubscriptions/exports/'.$filename.'";
                 link.download="'.$filename.'";
                 link.click();
                 window.setTimeout(function(){
                     jQuery("#downloadContainer").html("For security reason the file has been deleted.");
                     jQuery.ajax({
                         url:"index.php",
                         method:"get",
                         data : "option=com_alkasubscriptions&views=addresses"
                     });
                 },21000);
             })');
        }

		return $query;
	}

    static public function getAddresses()
	{
			$db     = JFactory::getDBO();
			$query  = $db->getQuery(true);
			$query->select(
					array(
							$db->quoteName('#__alkasubscriptions_address').'.*',
					)
			);
			$query->from($db->quoteName('#__alkasubscriptions_address'));
			$db->setQuery((string) $query);
			$items = $db->loadObjectList();
			return $items;
	}
}
