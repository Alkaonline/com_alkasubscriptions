DROP TABLE IF EXISTS `#__alkasubscriptions_subscription`;

CREATE TABLE `#__alkasubscriptions_subscription` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `createdDate` datetime NOT NULL,
  `createdBy` int(11) NOT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `modifiedBy` int(11) DEFAULT NULL,
  `startDate` date NOT NULL,
  `endDate` date DEFAULT NULL,
  `renewDate` date DEFAULT NULL,
  `forever` tinyint(1) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `idUser` int(11) NOT NULL,
  `idProduct` int(11) UNSIGNED NOT NULL,
  `idOrder` int(11) UNSIGNED NOT NULL,
  `idAddress` int(11) UNSIGNED DEFAULT NULL,
  `startIssueNumber` int(11) UNSIGNED DEFAULT NULL,
  `endIssueNumber` int(11) UNSIGNED DEFAULT NULL,
  `subscriptionType` varchar(3) NULL,
  `paid` tinyint(1) NOT NULL,
  `productOptions` text

  PRIMARY KEY (id)
)
	ENGINE =InnoDB
	DEFAULT CHARSET =utf8;

ALTER TABLE `#__alkasubscriptions_subscription`
ADD KEY `idUser`  (`idUser`),
ADD KEY `idProduct` (`idProduct`),
ADD KEY `idOrder` (`idOrder`),
ADD KEY `idAddress` (`idAddress`);

DROP TABLE IF EXISTS `#__alkasubscriptions_address`;

CREATE TABLE `#__alkasubscriptions_address` (
		`id` int(11) NOT NULL AUTO_INCREMENT,
	  `title` varchar(255) DEFAULT NULL,
	  `firstname` varchar(255) DEFAULT NULL,
	  `lastname` varchar(255) DEFAULT NULL,
	  `company` varchar(255) DEFAULT NULL,
	  `addressLine1` varchar(100) NOT NULL,
	  `addressLine2` varchar(100) DEFAULT NULL,
	  `city` varchar(45) NOT NULL,
	  `zip` varchar(25) DEFAULT NULL,
	  `country` varchar(25) DEFAULT NULL,
	  `createdDate` datetime NOT NULL,
	  `createdBy` int(11) NOT NULL,
	  `modifiedDate` datetime DEFAULT NULL,
	  `modifiedBy` int(11) DEFAULT NULL,
	  `enabled` tinyint(1) NOT NULL,
	  `note` text,
		`hikaAddressId` INT NULL DEFAULT NULL,
		`idUser` int(11) NOT NULL,

  PRIMARY KEY (id)
)
	ENGINE =InnoDB
	DEFAULT CHARSET =utf8;

DROP TABLE IF EXISTS `#__alkasubscriptions_mail_history`;

CREATE TABLE `#___alkasubscriptions_mail_history` (
  `id` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `recipient` varchar(255) NOT NULL,
  `idSubscription` int(11) NOT NULL,
  `sendDate` datetime NOT NULL
)
	ENGINE=InnoDB
	DEFAULT CHARSET=utf8;

ALTER TABLE `#__alkasubscriptions_subscription_mail_history`
ADD KEY `idSubscription`  (`idSubscription`);


CREATE TABLE `#___alkasubscriptions_deadlines` (
  `id` int(11) NOT NULL,
  `subscription` varchar(3) NOT NULL,
  `issue` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `#___alkasubscriptions_deadlines`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `#___alkasubscriptions_deadlines`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;