<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * HelloWorlds View
 *
 * @since  0.0.1
 */
class AlkaSubscriptionsViewAddresses extends JViewLegacy
{
	/**
	 * Display the Hello World view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 */
	function display($tpl = null)
	{
        $document = JFactory::getDocument();
		// Load JS files
		JHtml::_('jquery.framework');


        $document->addScript('https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js');
        $document->addScript('https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js');
		$document->addScript('components/com_alkasubscriptions/assets/js/URI.min.js');
		$document->addScript('components/com_alkasubscriptions/assets/js/notifications.js');
		$document->addScript('components/com_alkasubscriptions/assets/js/ajax.js');

		// Load CSS files
		$document->addStyleSheet('components/com_alkasubscriptions/assets/css/style.css');
        // Get application
		$app = JFactory::getApplication();
		$context = "alkasubscriptions.list.admin.addresses";
		// Get data from the model
		$this->items		= $this->get('Items');
		$this->pagination	= $this->get('Pagination');
        $this->filter_order 	= $app->getUserStateFromRequest($context.'filter_order', 'filter_order', 'id', 'cmd');
		$this->filter_order_Dir = $app->getUserStateFromRequest($context.'filter_order_Dir', 'filter_order_Dir', 'desc', 'cmd');
		$this->filterForm    	= $this->get('FilterForm');
		$this->activeFilters 	= $this->get('ActiveFilters');
        $this->addToolBar();
        $this->formAddress    = JForm::getInstance('addresses', JPATH_COMPONENT.'/models/forms/addresses.xml');

        JHtmlSidebar::addEntry(
            'Subscriptions',
            'index.php?option=com_alkasubscriptions&view=subscriptions',
            false
        );
        JHtmlSidebar::addEntry(
            'Addresses',
            'index.php?option=com_alkasubscriptions&view=addresses',
            true
        );
        $this->sidebar = JHtmlSidebar::render();

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode('<br />', $errors));

			return false;
		}

		// Display the template
		parent::display($tpl);
	}

    protected function addToolBar()
	{
		JToolBarHelper::title(JText::_('COM_ALKASUBSCRIPTIONS').' ['.JText::_('COM_ALKASUBSCRIPTIONS_ADDRESSES').']');
        JToolBarHelper::preferences('com_alkasubscriptions');
        JToolBarHelper::custom('create.address', 'new', 'new test', 'Create address', false);
	}
}
