<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
JHtml::_('formbehavior.chosen', 'select');

$listOrder     = $this->escape($this->filter_order);
$listDirn      = $this->escape($this->filter_order_Dir);
?>
<form action="index.php?option=com_alkasubscriptions&view=addresses" method="post" id="adminForm" name="adminForm">
    <?php if (!empty( $this->sidebar)) : ?>
    	<div id="j-sidebar-container" class="span2">
    		<?php echo $this->sidebar; ?>
    	</div>
    	<div id="j-main-container" class="span10">
    <?php else : ?>
    	<div id="j-main-container">
    <?php endif; ?>
    <div class="row-fluid">
		<div class="span12">
			<?php
				echo JLayoutHelper::render(
					'joomla.searchtools.default',
					array('view' => $this)
				);
			?>
		</div>
	</div>
	<table class="table table-striped table-hover">
		<thead>
		<tr>
            <th><?php echo JHtml::_('grid.sort', 'COM_ALKASUBSCRIPTIONS_ID', 'id', $listDirn, $listOrder); ?></th>
            <th><?php echo JHtml::_('grid.sort', 'COM_ALKASUBSCRIPTIONS_TITLE', 'title', $listDirn, $listOrder); ?></th>
            <th><?php echo JHtml::_('grid.sort', 'COM_ALKASUBSCRIPTIONS_FIRSTNAME', 'firstname', $listDirn, $listOrder); ?></th>
            <th><?php echo JHtml::_('grid.sort', 'COM_ALKASUBSCRIPTIONS_LASTNAME', 'lastname', $listDirn, $listOrder); ?></th>
            <th><?php echo JText::_('COM_ALKASUBSCRIPTIONS_ADDRESS_LINE_1'); ?></th>
            <th><?php echo JText::_('COM_ALKASUBSCRIPTIONS_ADDRESS_LINE_2'); ?></th>
            <th><?php echo JText::_('COM_ALKASUBSCRIPTIONS_CITY'); ?></th>
            <th><?php echo JText::_('COM_ALKASUBSCRIPTIONS_ZIP'); ?></th>
            <th><?php echo JText::_('COM_ALKASUBSCRIPTIONS_COUNTRY'); ?></th>
            <th class="center"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_ACTIONS'); ?></th>
            <th class="center"><?php echo JHtml::_('grid.sort', 'COM_ALKASUBSCRIPTIONS_STATE', 'enabled', $listDirn, $listOrder); ?></th>
		</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="11">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
		</tfoot>
		<tbody>
			<?php if (!empty($this->items)) : ?>
				<?php foreach ($this->items as $i => $row) : ?>

					<tr>
						<td>
							<?php echo $row->id; ?>
						</td>
                        <td><?php echo $row->title ?></td>
                        <td><?php echo $row->firstname ?></td>
                        <td><?php echo $row->lastname ?></td>
                        <td><?php echo $row->addressLine1 ?></td>
                        <td><?php echo $row->addressLine2 ?></td>
                        <td><?php echo $row->city ?></td>
                        <td><?php echo $row->zip ?></td>
                        <td><?php echo $row->country ?></td>
                        <td class="center">
                            <button type="button" class="btn btn-primary" href="javascript:void(0)" onclick="setAddress(<?php echo $row->id ?>)"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_EDIT'); ?></button>
                        </td>
                        <td class="center">
                            <?php if($row->enabled == 1) { ?>
                                <a class="btn btn-success" href="javascript:void(0)" onclick="checkLink(<?php echo $row->id ?>)"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_ENABLED'); ?></a>
                            <?php } else { ?>
                                <a class="btn btn-warning" href="javascript:void(0)" onclick="setAddressState(<?php echo $row->id ?>,1)"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_DISABLED'); ?></a>
                            <?php } ?>
                        </td>
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
	</table>
    <input type="hidden" name="task" value=""/>
    <input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>"/>
    <input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>"/>
    <?php echo JHtml::_('form.token'); ?>
</form>


<div id="set-address-modal" class="modal hide fade">
  <form method="post" id="set-address-form" class="form-horizontal" enctype="multipart/form-data"  action="<?php echo JRoute::_('index.php'); ?>">
    <div class="modal-header">
      <a type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</a>
      <h3><?php echo JText::_('COM_ALKASUBSCRIPTIONS_ADD_ADDRESS'); ?></h3>
    </div>
    <div class="modal-body" style="overflow-y: auto;">
      <?php echo $this->formAddress->renderFieldset('default'); ?>
    </div>
    <div class="modal-footer">
      <a class="btn" data-dismiss="modal" aria-hidden="true"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_CLOSE'); ?></a>
      <button type="submit" class="btn btn-primary"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_SUBMIT'); ?></button>
    </div>
    <input type="hidden" name="controller" value="addresses"/>
    <input type="hidden" name="option" value="com_alkasubscriptions"/>
    <input type="hidden" name="task"  class="task" value="setAddress"/>
</div>
  </form>
</div>
<div id="link-address-modal" class="modal hide fade" data-idsubscription="" data-idaddress="">
  <!-- <form method="post" id="link-address-form" class="form-vertical" enctype="multipart/form-data"  action="<?php echo JRoute::_('index.php'); ?>"> -->
    <div class="modal-header">
      <a type="button" class="close" data-dismiss="modal" aria-hidden="true" style="text-align: center;">&times;</a>
      <h3><?php echo JText::_('COM_ALKASUBSCRIPTIONS_LINK_ADDRESS'); ?></h3>
    </div>
		<button type="button" onclick="setAddress()" class="btn btn-primary" style="margin:10px;"><i class="fa fa-plus" aria-hidden="true"></i> <?php echo JText::_('COM_ALKASUBSCRIPTIONS_ADD_ADDRESS'); ?></button>
    <div class="modal-body" style="overflow-y: auto;">
      <table id="addresses-modal-table" class="table table-striped table-hover">
        <thead>
          <th>ID</th>
          <th>Title</th>
          <th>Firstname</th>
          <th>Lastname</th>
          <th>Address line 1</th>
          <th>Address line 2</th>
          <th>City</th>
          <th>Zip</th>
          <th>Country</th>
          <th>Actions</th>
        </thead>
        <tbody>
          <?php foreach ($this->addresses as $address) { ?>
          <tr>
            <td><?php echo $address->id ?></td>
            <td><?php echo $address->title ?></td>
            <td><?php echo $address->firstname ?></td>
            <td><?php echo $address->lastname ?></td>
            <td><?php echo $address->addressLine1 ?></td>
            <td><?php echo $address->addressLine2 ?></td>
            <td><?php echo $address->city ?></td>
            <td><?php echo $address->zip ?></td>
            <td><?php echo $address->country ?></td>
            <td>
              <button id="btn-link-<?php echo $address->id ?>" class="btn btn-primary" onclick="linkAddress(<?php echo $address->id ?>)"><i class="fa fa-link" aria-hidden="true"></i> <?php echo JText::_('COM_ALKASUBSCRIPTIONS_LINK'); ?></button>
            </td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
    <div class="modal-footer">
      <a class="btn" data-dismiss="modal" aria-hidden="true"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_CLOSE'); ?></a>
    </div>
    <input type="hidden" name="controller" value="addresses"/>
    <input type="hidden" name="option" value="com_alkasubscriptions"/>
    <input type="hidden" name="task"  class="task" value="link"/>
  <!-- </form> -->
</div>
<script>
jQuery(document).ready(function(){
    jQuery('button[onclick="Joomla.submitbutton(\'create.address\');"]').attr('onclick','setAddress()');
});
</script>
