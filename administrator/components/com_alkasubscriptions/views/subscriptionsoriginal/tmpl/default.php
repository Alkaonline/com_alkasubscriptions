<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
?>
<ul class="nav nav-tabs">
	<li class="<?php if(JFactory::getApplication()->input->get('tab','subscriptions',null)=='subscriptions'){echo 'active';} ?>">
		<a data-toggle="tab" href="#subscriptions"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_SUBSCRIPTIONS'); ?></a>
	</li>
	<li class="<?php if(JFactory::getApplication()->input->get('tab',null,null)=='addresses'){echo 'active';} ?>">
		<a data-toggle="tab" href="#addresses"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_ADDRESSES'); ?></a>
	</li>
</ul>

<div class="tab-content">
	<div id="subscriptions" class="tab-pane fade <?php if(JFactory::getApplication()->input->get('tab','subscriptions',null)=='subscriptions'){echo 'in active';} ?>">

		<form action="index.php?option=com_alkasubscriptions&view=AlkaSubscriptions" method="post" id="adminForm" name="adminForm">
			<table id="subscriptions-table" class="table table-striped table-hover">
				<thead>
					<th width="1%"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_ID'); ?></th>
					<th><?php echo JText::_('COM_ALKASUBSCRIPTIONS_NAME'); ?></th>
                    <th><?php echo JText::_('Code'); ?></th>
					<th class="center"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_TYPE'); ?></th>
					<th  style="white-space:nowrap;"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_START_DATE'); ?></th>
					<th  style="white-space:nowrap;"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_END_DATE'); ?></th>
					<th class="center"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_ORDER'); ?></th>
					<th class="center"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_PAID'); ?></th>
					<!--<th class="center"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_FOREVER'); ?></th>-->
					<th class="center"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_OWNER'); ?></th>
					<th class="center"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_FIRSTNAME'); ?></th>
					<th class="center"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_LASTNAME'); ?></th>
					<th class="center"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_COMPANY'); ?></th>
					<th class="center"><?php echo JText::_('JLIB_RULES_GROUPS'); ?></th>
					<th><?php echo JText::_('Address'); ?></th>
                    <th><?php echo JText::_('Address2'); ?></th>
                    	<th><?php echo JText::_('Zip'); ?></th>
                        	<th><?php echo JText::_('City'); ?></th>
                            	<th><?php echo JText::_('Country'); ?></th>
					<th class="center"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_ADDRESS'); ?></th>
					<th class="center"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_STATE'); ?></th>
                    	<th class="center"><?php echo JText::_('Notification'); ?></th>
				</thead>
				<tbody>
					<?php foreach ($this->subscriptions as $subscription) { ?>
					<tr>
						<td><?php echo $subscription->idSub ?></td>
						<td>
							<strong>
								<?php echo $subscription->name ?>
							</strong>
                        </td>
                            <td>
							<?php echo $subscription->productCode; ?>
						</td>
						<td class="center">
							<?php
								$type = @unserialize($subscription->productOptions);
								if(isset($type[0]->characteristic_value)){echo $type[0]->characteristic_value;}else{echo '-';}
							?>
						</td>
						<td><?php echo $subscription->startDate ?></td>
						<td><?php echo $subscription->endDate ?></td>
						<td class="center"><?php  if ($subscription->idOrder){ ?><a href="index.php?option=com_hikashop&ctrl=order&task=edit&cid[]=<?php echo $subscription->idOrder ?>" target="_blank"><?php echo $subscription->idOrder ?></a><?php } ?></td>
						<td class="center"><?php echo ($subscription->paid)?'<span style="display:none;">1</span><span class="text-success"><span class="icon-publish"></span></span>':'<span style="display:none;">0</span><span class="text-danger"><span class="icon-unpublish"></span></span>'; ?></td>
						<!--<td class="center"><?php echo $subscription->forever ?></td>-->
						<td class="center"><?php echo $subscription->username ?></td>
						<td class="center"><?php echo $subscription->firstname ?></td>
						<td class="center"><?php echo $subscription->lastname ?></td>
						<td class="center"><?php echo $subscription->company ?></td>
						<td class="center"><?php
							$groups =  explode(';',$subscription->groups);
							echo "<span class='label label-default'>".implode("</span>&nbsp;<span class='label label-default'>",$groups)."</span>";
						?></td>
						<td><?php echo $subscription->addressLine1 ?></td>
						<td><?php echo $subscription->addressLine2 ?></td>
						<td><?php 		echo $subscription->zip ?></td>
						<td><?php 		echo $subscription->city  ?></td>
							<td><?php 	echo $subscription->country ?></td>
						</td>
						<td class="center"><a class="btn btn-default" onclick="linkAddressShow(<?php echo $subscription->idSub ?>,<?php echo $subscription->idAdd ?>)"><span class="icon-link"></span></a></td>
						<?php if($subscription->enabled == 1) {
							echo '<td class="center"><a class="btn btn-default" href="index.php?option=com_alkasubscriptions&controller=subscriptions&task=setState&id=' . $subscription->idSub . '&state=' . $subscription->enabled . '"><span style="display:none;">1</span><span class="icon-publish"></span></a></td>';
						} else {
							echo '<td class="center"><a class="btn btn-default" href="index.php?option=com_alkasubscriptions&controller=subscriptions&task=setState&id=' . $subscription->idSub . '&state=' . $subscription->enabled . '"><span style="display:none;">0</span><span class="icon-unpublish"></span></a></td>';
						} ?>
                        <?php if($subscription->sendNotification == 1) {
							echo '<td class="center"><a class="btn btn-default" href="index.php?option=com_alkasubscriptions&controller=subscriptions&task=setNotif&id=' . $subscription->idSub . '&state=' . $subscription->sendNotification . '"><span style="display:none;">1</span><span class="icon-publish"></span></a></td>';
						} else {
							echo '<td class="center"><a class="btn btn-default" href="index.php?option=com_alkasubscriptions&controller=subscriptions&task=setNotif&id=' . $subscription->idSub . '&state=' . $subscription->sendNotification . '"><span style="display:none;">0</span><span class="icon-unpublish"></span></a></td>';
						} ?>
					</tr>
					<?php } ?>
				</tbody>
			</table>
			<input type="hidden" name="task" value=""/>
			<input type="hidden" name="boxchecked" value="0"/>
			<?php echo JHtml::_('form.token'); ?>
		</form>
	</div>
	<div id="addresses" class="tab-pane <?php if(JFactory::getApplication()->input->get('tab',null,null)=='addresses'){echo 'in active';} ?> fade">
		<div class="row-fluid">
			<div class="span1">
				<button type="button" onclick="setAddress()" class="btn btn-primary" style="margin-bottom: 10px;"><i class="fa fa-plus" aria-hidden="true"></i> Add Address </button>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span12">
				<table id="addresses-table" class="table table-striped table-hover">
					<thead>
						<th><?php echo JText::_('COM_ALKASUBSCRIPTIONS_ID'); ?></th>
						<th><?php echo JText::_('COM_ALKASUBSCRIPTIONS_TITLE'); ?></th>
						<th><?php echo JText::_('COM_ALKASUBSCRIPTIONS_FIRSTNAME'); ?></th>
						<th><?php echo JText::_('COM_ALKASUBSCRIPTIONS_LASTNAME'); ?></th>
						<th><?php echo JText::_('COM_ALKASUBSCRIPTIONS_ADDRESS_LINE_1'); ?></th>
						<th><?php echo JText::_('COM_ALKASUBSCRIPTIONS_ADDRESS_LINE_2'); ?></th>
						<th><?php echo JText::_('COM_ALKASUBSCRIPTIONS_CITY'); ?></th>
						<th><?php echo JText::_('COM_ALKASUBSCRIPTIONS_ZIP'); ?></th>
						<th><?php echo JText::_('COM_ALKASUBSCRIPTIONS_COUNTRY'); ?></th>
						<th class="center"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_ACTIONS'); ?></th>
						<th class="center"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_STATE'); ?></th>
					</thead>
					<tbody>
						<?php foreach ($this->addresses as $address) { ?>
						<tr>
							<td><?php echo $address->id ?></td>
							<td><?php echo $address->title ?></td>
							<td><?php echo $address->firstname ?></td>
							<td><?php echo $address->lastname ?></td>
							<td><?php echo $address->addressLine1 ?></td>
							<td><?php echo $address->addressLine2 ?></td>
							<td><?php echo $address->city ?></td>
							<td><?php echo $address->zip ?></td>
							<td><?php echo $address->country ?></td>
							<td class="center">
								<button class="btn btn-primary" href="javascript:void(0)" onclick="setAddress(<?php echo $address->id ?>)"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_EDIT'); ?></button>
							</td>
							<td class="center">
								<?php if($address->enabled == 1) { ?>
									<a class="btn btn-success" href="javascript:void(0)" onclick="checkLink(<?php echo $address->id ?>)"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_ENABLED'); ?></a>
								<?php } else { ?>
									<a class="btn btn-warning" href="javascript:void(0)" onclick="setAddressState(<?php echo $address->id ?>,1)"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_DISABLED'); ?></a>
								<?php } ?>
							</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div id="set-address-modal" class="modal hide fade">
  <form method="post" id="set-address-form" class="form-horizontal" enctype="multipart/form-data"  action="<?php echo JRoute::_('index.php'); ?>">
    <div class="modal-header">
      <a type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</a>
      <h3><?php echo JText::_('COM_ALKASUBSCRIPTIONS_ADD_ADDRESS'); ?></h3>
    </div>
    <div class="modal-body" style="overflow-y: auto;">
      <?php echo $this->formAddress->renderFieldset('default'); ?>
    </div>
    <div class="modal-footer">
      <a class="btn" data-dismiss="modal" aria-hidden="true"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_CLOSE'); ?></a>
      <button type="submit" class="btn btn-primary"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_SUBMIT'); ?></button>
    </div>
    <input type="hidden" name="controller" value="addresses"/>
    <input type="hidden" name="option" value="com_alkasubscriptions"/>
    <input type="hidden" name="task"  class="task" value="setAddress"/>
  </form>
</div>

<div id="link-address-modal" class="modal hide fade" data-idsubscription="" data-idaddress="">
  <!-- <form method="post" id="link-address-form" class="form-vertical" enctype="multipart/form-data"  action="<?php echo JRoute::_('index.php'); ?>"> -->
    <div class="modal-header">
      <a type="button" class="close" data-dismiss="modal" aria-hidden="true" style="text-align: center;">&times;</a>
      <h3><?php echo JText::_('COM_ALKASUBSCRIPTIONS_LINK_ADDRESS'); ?></h3>
    </div>
		<button type="button" onclick="setAddress()" class="btn btn-primary" style="margin:10px;"><i class="fa fa-plus" aria-hidden="true"></i> <?php echo JText::_('COM_ALKASUBSCRIPTIONS_ADD_ADDRESS'); ?></button>
    <div class="modal-body" style="overflow-y: auto;">
      <table id="addresses-modal-table" class="table table-striped table-hover">
        <thead>
          <th>ID</th>
          <th>Title</th>
          <th>Firstname</th>
          <th>Lastname</th>
          <th>Address line 1</th>
          <th>Address line 2</th>
          <th>City</th>
          <th>Zip</th>
          <th>Country</th>
          <th>Actions</th>
        </thead>
        <tbody>
          <?php foreach ($this->addresses as $address) { ?>
          <tr>
            <td><?php echo $address->id ?></td>
            <td><?php echo $address->title ?></td>
            <td><?php echo $address->firstname ?></td>
            <td><?php echo $address->lastname ?></td>
            <td><?php echo $address->addressLine1 ?></td>
            <td><?php echo $address->addressLine2 ?></td>
            <td><?php echo $address->city ?></td>
            <td><?php echo $address->zip ?></td>
            <td><?php echo $address->country ?></td>
            <td>
              <button id="btn-link-<?php echo $address->id ?>" class="btn btn-primary" onclick="linkAddress(<?php echo $address->id ?>)"><i class="fa fa-link" aria-hidden="true"></i> <?php echo JText::_('COM_ALKASUBSCRIPTIONS_LINK'); ?></button>
            </td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
    <div class="modal-footer">
      <a class="btn" data-dismiss="modal" aria-hidden="true"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_CLOSE'); ?></a>
    </div>
    <input type="hidden" name="controller" value="addresses"/>
    <input type="hidden" name="option" value="com_alkasubscriptions"/>
    <input type="hidden" name="task"  class="task" value="link"/>
  <!-- </form> -->
</div>
