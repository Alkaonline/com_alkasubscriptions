<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * HelloWorlds View
 *
 * @since  0.0.1
 */
class AlkaSubscriptionsViewSubscriptions extends JViewLegacy
{
	/**
	 * Display the Hello World view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 */
	function display($tpl = null)
	{
		$document = JFactory::getDocument();
		// Load JS files
		JHtml::_('jquery.framework');
        JHtml::_('jquery.ui',array('core','easing'));
		$document->addScript('https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js');
		$document->addScript('https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js');
		$document->addScript('https://cdn.datatables.net/buttons/1.3.1/js/buttons.colVis.min.js');
		$document->addScript('https://cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js');
		$document->addScript('https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js');

		$document->addScript('components/com_alkasubscriptions/assets/js/URI.min.js');
		$document->addScript('components/com_alkasubscriptions/assets/js/notifications.js');
		$document->addScript('components/com_alkasubscriptions/assets/js/ajax.js');

		// Load CSS files
		$document->addStyleSheet('components/com_alkasubscriptions/assets/css/style.css');
		$document->addStyleSheet('https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css');
		$document->addStyleSheet('https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css');
        $app = JFactory::getApplication();
		$context = "alkasubscriptions.list.admin.addresses";
		// Get data from the model
		$this->items		= $this->get('Items');
      //   foreach($this->items as $k=>$item){
      //       $db     = JFactory::getDBO();
			// $query  = $db->getQuery(true);
			// $query->select(
			// 		array(
			// 				$db->quoteName('#__hikashop_order').'.*',
			// 		)
			// );
			// $query->from($db->quoteName('#__alkasubscriptions_subscription_order'));
      //       $query->join('LEFT',$db->quoteName('#__hikashop_order').' ON ('.$db->quoteName('#__hikashop_order.order_id').' = '.$db->quoteName('#__alkasubscriptions_subscription_order.id_order').')');
      //       $query->where($db->quoteName('#__alkasubscriptions_subscription_order.id_subscription').'='.$db->quote($item->idSub));
      //       $query->orderBy($db->quoteName('#__hikashop_order.order_id').' DESC');
			// $db->setQuery($query);
      //       $result = $db->loadObjectList();
      //       $this->items[$k]->orders = array();
      //       $this->items[$k]->orders = $db->loadObjectList();
      //
      //   }
		$this->pagination	    = $this->get('Pagination');
        $this->filter_order 	= $app->getUserStateFromRequest($context.'filter_order', 'filter_order', 'idSub', 'cmd');
		$this->filter_order_Dir = $app->getUserStateFromRequest($context.'filter_order_Dir', 'filter_order_Dir', 'desc', 'cmd');
		$this->filterForm    	= $this->get('FilterForm');
		$this->activeFilters 	= $this->get('ActiveFilters');

        $this->addresses 			= $this->get('Addresses');

		// Load forms
		$this->formAddress    = JForm::getInstance('addresses', JPATH_COMPONENT.'/models/forms/addresses.xml');
        $this->formDate       = JForm::getInstance('subscription', JPATH_COMPONENT.'/models/forms/date.xml');
        $this->formIssue       = JForm::getInstance('subscription_issues', JPATH_COMPONENT.'/models/forms/issue.xml');
        JHtmlSidebar::addEntry(
            'Subscriptions',
            'index.php?option=com_alkasubscriptions&view=subscriptions',
            true
        );
        JHtmlSidebar::addEntry(
            'Addresses',
            'index.php?option=com_alkasubscriptions&view=addresses',
            false
        );
        $this->sidebar = JHtmlSidebar::render();

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode('<br />', $errors));

			return false;
		}

		// Set the toolbar
		$this->addToolBar();



		// Display the template
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function addToolBar()
	{
		JToolBarHelper::title(JText::_('COM_ALKASUBSCRIPTIONS'));
		JToolBarHelper::preferences('com_alkasubscriptions');
        JToolbarHelper::custom('subscriptions.exports', 'refresh.png', 'refresh_f2.png', 'Export subscriptions (as currently filtered)', false);
	}
}
