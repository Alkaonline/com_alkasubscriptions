<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
JHtml::_('formbehavior.chosen', 'select');

$listOrder     = $this->escape($this->filter_order);
$listDirn      = $this->escape($this->filter_order_Dir);
$app = &JFactory::getApplication();
$params = JComponentHelper::getParams('com_alkasubscriptions');
?>
<form action="index.php?option=com_alkasubscriptions&view=subscriptions" method="post" id="adminForm" name="adminForm">
<?php if (!empty( $this->sidebar)) : ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php else : ?>
	<div id="j-main-container">
<?php endif; ?>
    <div class="row-fluid">
		<div class="span12">
			<?php
				echo JLayoutHelper::render(
					'joomla.searchtools.default',
					array('view' => $this)
				);
			?>
		</div>
	</div>
	<table class="table table-striped table-hover">
		<thead>
		<tr>
            <th><?php echo JHtml::_('grid.sort', 'COM_ALKASUBSCRIPTIONS_ID', 'idSub', $listDirn, $listOrder); ?></th>
            <th><?php echo JHtml::_('grid.sort', 'COM_ALKASUBSCRIPTIONS_NAME', '->name', $listDirn, $listOrder); ?></th>
            <th><?php echo JHtml::_('grid.sort', 'CODE', 'productCode', $listDirn, $listOrder); ?></th>
            <th class="center"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_TYPE'); ?></th>
            <?php if ($params->get('mode','duration')=='duration') : ?>
                <th style="white-space:nowrap;"><?php echo JHtml::_('grid.sort', 'COM_ALKASUBSCRIPTIONS_START_DATE', 'startDate', $listDirn, $listOrder); ?></th>
                <th style="white-space:nowrap;"><?php echo JHtml::_('grid.sort', 'COM_ALKASUBSCRIPTIONS_END_DATE', 'endDate', $listDirn, $listOrder); ?></th>
            <?php else: ?>
                <th class="center"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_STARTISSUE'); ?></th>
                <th class="center"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_ENDISSUE'); ?></th>
            <?php endif; ?>
            <th class="center"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_ORDER'); ?></th>
            <th class="center"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_PAID'); ?></th>
            <th class="center"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_OWNER'); ?></th>
            <th class="center"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_FIRSTNAME'); ?></th>
            <th class="center"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_LASTNAME'); ?></th>
            <th class="center"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_COMPANY'); ?></th>
            <th class="center"><?php echo JText::_('JLIB_RULES_GROUPS'); ?></th>
            <th><?php echo JText::_('COM_ALKASUBSCRIPTIONS_ADDRESS_LINE_1'); ?></th>
            <th><?php echo JText::_('COM_ALKASUBSCRIPTIONS_ADDRESS_LINE_2'); ?></th>
                <th><?php echo JText::_('Zip'); ?></th>
                    <th><?php echo JText::_('City'); ?></th>
                        <th><?php echo JText::_('Country'); ?></th>
            <th class="center"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_ADDRESS'); ?></th>
            <th class="center"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_STATE'); ?></th>
                <th class="center"><?php echo JText::_('Notification'); ?></th>
                <th class="center"><?php echo JText::_('Delete'); ?></th>
		</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="21">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
		</tfoot>
		<tbody>
			<?php if (!empty($this->items)) : ?>
				<?php foreach ($this->items as $i => $row) :
                    $end = new DateTime($row->endDate);
                    $start = new DateTime($row->startDate);
                    // $row = AlkaSubscriptionsHelper::getSubscriptionNameDynamic($row) ;
                    ?>

					<tr id="row-<?php echo $row->idSub ?>">
                        <td><?php echo $row->idSub ?></td>
						<td>
							<strong>
								<?php echo $row->name ? $row->name : $row->initialName ;?>
							</strong>
                        </td>
                            <td style="white-space:nowrap;">
              <?php echo $row->productCode ? $row->productCode : $row->initialProductCode ;?>

						</td style="white-space:nowrap;">
						<td style="white-space:nowrap;" class="center">
							<?php
								$type = @unserialize($row->productOptions);
								if(isset($type[0]->characteristic_value)){
                                    echo $type[0]->characteristic_value;
                                }else if(isset($type["type"])){
                                    echo $type["type"];
                                }else{
                                    echo '-';
                                }
                $orders = AlkasubscriptionsHelper::getRenewOrdersBySubscription($row,true);
							?>
						</td>
                        <?php if ($params->get('mode','duration')=='duration') : ?>
						    <td style="white-space:nowrap;" id="startDate-<?php echo $row->idSub ?>"><a href="javascript:void(0);" onclick="prepareDateForm('<?php echo $start->format('Y-m-d'); ?>', '<?php echo $end->format('Y-m-d'); ?>', <?php echo $row->idSub ?>);"><?php echo $start->format('d/m/Y'); ?></a></td>
						    <td style="white-space:nowrap;" id="endDate-<?php echo $row->idSub ?>"><a href="javascript:void(0);" onclick="prepareDateForm('<?php echo $start->format('Y-m-d'); ?>', '<?php echo $end->format('Y-m-d'); ?>', <?php echo $row->idSub ?>);"><?php echo $end->format('d/m/Y'); ?></a></td>
              <?php elseif(sizeof($orders)<=1): ?>
                            <td style="white-space:nowrap;" class="center" id="startIssue-<?php echo $row->idSub ?>"><a href="javascript:void(0);" onclick="prepareIssueForm('<?php echo $row->startIssueNumber;?>','<?php echo $row->endIssueNumber; ?>',<?php echo $row->idSub; ?>);"><?php echo $row->startIssueNumber ?></a></td>
                            <td style="white-space:nowrap;" class="center" id="endIssue-<?php echo $row->idSub ?>"><a href="javascript:void(0);" onclick="prepareIssueForm('<?php echo $row->startIssueNumber;?>','<?php echo $row->endIssueNumber; ?>',<?php echo $row->idSub; ?>);"><?php echo $row->endIssueNumber ?></a></td>
                        <?php else: ?>
                          <td style="white-space:nowrap; opacity:0.6;" class="center text-muted" id="startIssue-<?php echo $row->idSub ?>"><?php echo $row->startIssueNumber ?></td>
                          <td style="white-space:nowrap; opacity:0.6;" class="center text-muted" id="endIssue-<?php echo $row->idSub ?>"><?php echo $row->endIssueNumber ?></td>
                        <?php endif; ?>

                        <td style="white-space:nowrap;" class="center">
                            <?php
                            if (sizeof($orders)>0){ ?>
                                <div class="btn-group">
                                    <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                                        Orders
                                    <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <?php  foreach($orders as $order){
                                            $date = new DateTime("@$order->order_created"); ?>
                                            <li><a href="index.php?option=com_hikashop&ctrl=order&task=edit&cid[]=<?php echo $order->order_id ?>" target="_blank">
                                                <?php echo $order->orderType == 'initial' ? '<span class="label label-default">Initial</span> : ' : '<span class="label label-info">Renew</span> : '; ?>
                                                <?php echo $order->order_id ?> (<?php echo $date->format(JText::_('DATE_FORMAT_LC')); ?>)
                                                <?php echo ($order->productName && $order->startIssue && $order->endIssue) ? ' <strong>'.$order->productName.'</strong> #'.$order->startIssue.' -> #'.$order->endIssue : ''; ?>
                                            </a></li>
                                        <?php } ?>
                                    </ul>
                                </div>

                            <?php } ?>
                        </td>
						<td style="white-space:nowrap;" class="center"><?php echo ($row->paid)?'<span style="display:none;">1</span><span class="text-success"><span class="icon-publish"></span></span>':'<span style="display:none;">0</span><span class="text-danger"><span class="icon-unpublish"></span></span>'; ?></td>
						<!--<td class="center"><?php echo $row->forever ?></td>-->
						<td style="white-space:nowrap;" class="center"><?php echo $row->username ?></td>
						<td class="center"><?php echo $row->firstname ?></td>
						<td class="center"><?php echo $row->lastname ?></td>
						<td class="center"><?php echo $row->company ?></td>
						<td class="center"><?php
							$groups =  explode(';',$row->groups);
              $groups = array_unique($groups);
							echo "<span class='label label-default'>".implode("</span>&nbsp;<span class='label label-default'>",$groups)."</span>";
						?></td>
						<td><?php echo $row->addressLine1 ?></td>
						<td><?php echo $row->addressLine2 ?></td>
						<td style="white-space:nowrap;"><?php 		echo $row->zip ?></td>
						<td style="white-space:nowrap;"><?php 		echo $row->city  ?></td>
							<td style="white-space:nowrap;"><?php 	echo $row->country ?></td>
						</td>
						<td style="white-space:nowrap;" class="center"><a class="btn btn-default" onclick="linkAddressShow(<?php echo $row->idSub ?>,<?php echo $row->idAdd ?>)"><span class="icon-link"></span></a></td>
						<?php if($row->enabled == 1) {
							echo '<td class="center"><a class="btn btn-default" href="index.php?option=com_alkasubscriptions&controller=subscriptions&task=setState&id=' . $row->idSub . '&state=' . $row->enabled . '"><span style="display:none;">1</span><span class="icon-publish"></span></a></td>';
						} else {
							echo '<td class="center"><a class="btn btn-default" href="index.php?option=com_alkasubscriptions&controller=subscriptions&task=setState&id=' . $row->idSub . '&state=' . $row->enabled . '"><span style="display:none;">0</span><span class="icon-unpublish"></span></a></td>';
						} ?>
                        <?php if($row->sendNotification == 1) {
							echo '<td class="center"><a class="btn btn-default" href="index.php?option=com_alkasubscriptions&controller=subscriptions&task=setNotif&id=' . $row->idSub . '&state=' . $row->sendNotification . '"><span style="display:none;">1</span><span class="icon-publish"></span></a></td>';
						} else {
							echo '<td class="center"><a class="btn btn-default" href="index.php?option=com_alkasubscriptions&controller=subscriptions&task=setNotif&id=' . $row->idSub . '&state=' . $row->sendNotification . '"><span style="display:none;">0</span><span class="icon-unpublish"></span></a></td>';
						} ?>
						<td><a class="btn btn-default" href="javascript:void(0)" onclick="trashedSub(<?php echo $row->idSub; ?>)"><span class="icon-trash"></span></a></td>
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
	</table>
    <input type="hidden" name="task" value=""/>
    <input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>"/>
    <input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>"/>
    <?php echo JHtml::_('form.token'); ?>
</div>
</form>

<div id="set-date-modal" class="modal hide fade" style="width:30%; margin-left:-15%;">
  <form method="post" id="set-date-form" class="form-horizontal" enctype="multipart/form-data" onsubmit="submitDateForm(); return false;" action="<?php echo JRoute::_('index.php'); ?>">
    <div class="modal-header">
      <a type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</a>
      <h3><?php echo JText::_('COM_ALKASUBSCRIPTIONS_SET_DATE'); ?></h3>
    </div>
    <div class="modal-body">
        <br/>
      <?php echo $this->formDate->renderFieldset('default'); ?>
    </div>
    <div class="modal-footer">
      <a class="btn" data-dismiss="modal" aria-hidden="true"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_CLOSE'); ?></a>
      <button type="button" class="btn btn-primary" onclick="submitDateForm(); "><?php echo JText::_('COM_ALKASUBSCRIPTIONS_SUBMIT'); ?></button>
    </div>
    <input type="hidden" name="controller" value="subscriptions"/>
    <input type="hidden" name="option" value="com_alkasubscriptions"/>
    <input type="hidden" name="task"  class="task" value="setDate"/>
  </form>
</div>

<div id="set-issue-modal" class="modal hide fade" style="width:30%; margin-left:-15%;">
  <form method="post" id="set-issue-form" class="form-horizontal" enctype="multipart/form-data" onsubmit="submitIssueForm(); return false;" action="<?php echo JRoute::_('index.php'); ?>">
    <div class="modal-header">
      <a type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</a>
      <h3><?php echo JText::_('COM_ALKASUBSCRIPTIONS_SET_ISSUE'); ?></h3>
    </div>
    <div class="modal-body">
        <br/>
      <?php echo $this->formIssue->renderFieldset('default'); ?>
    </div>
    <div class="modal-footer">
      <a class="btn" data-dismiss="modal" aria-hidden="true"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_CLOSE'); ?></a>
      <button type="button" class="btn btn-primary" onclick="submitIssueForm(); "><?php echo JText::_('COM_ALKASUBSCRIPTIONS_SUBMIT'); ?></button>
    </div>
    <input type="hidden" name="controller" value="subscriptions"/>
    <input type="hidden" name="option" value="com_alkasubscriptions"/>
    <input type="hidden" name="task"  class="task" value="setIssue"/>
  </form>
</div>

<div id="set-address-modal" class="modal hide fade">
  <form method="post" id="set-address-form" class="form-horizontal" enctype="multipart/form-data"  action="<?php echo JRoute::_('index.php'); ?>">
    <div class="modal-header">
      <a type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</a>
      <h3><?php echo JText::_('COM_ALKASUBSCRIPTIONS_ADD_ADDRESS'); ?></h3>
    </div>
    <div class="modal-body" style="overflow-y: auto;">
      <?php echo $this->formAddress->renderFieldset('default'); ?>
    </div>
    <div class="modal-footer">
      <a class="btn" data-dismiss="modal" aria-hidden="true"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_CLOSE'); ?></a>
      <button type="submit" class="btn btn-primary" ><?php echo JText::_('COM_ALKASUBSCRIPTIONS_SUBMIT'); ?></button>
    </div>
    <input type="hidden" name="controller" value="addresses"/>
    <input type="hidden" name="option" value="com_alkasubscriptions"/>
    <input type="hidden" name="task"  class="task" value="setAddress"/>
  </form>
</div>

<div id="link-address-modal" class="modal hide fade" data-idsubscription="" data-idaddress="">
  <!-- <form method="post" id="link-address-form" class="form-vertical" enctype="multipart/form-data"  action="<?php echo JRoute::_('index.php'); ?>"> -->
    <div class="modal-header">
      <a type="button" class="close" data-dismiss="modal" aria-hidden="true" style="text-align: center;">&times;</a>
      <h3><?php echo JText::_('COM_ALKASUBSCRIPTIONS_LINK_ADDRESS'); ?></h3>
    </div>
		<button type="button" onclick="setAddress()" class="btn btn-primary" style="margin:10px;"><i class="fa fa-plus" aria-hidden="true"></i> <?php echo JText::_('COM_ALKASUBSCRIPTIONS_ADD_ADDRESS'); ?></button>
    <div class="modal-body" style="overflow-y: auto;">
      <table id="addresses-modal-table" class="table table-striped table-hover">
        <thead>
          <th>ID</th>
          <th>Title</th>
          <th>Firstname</th>
          <th>Lastname</th>
          <th>Address line 1</th>
          <th>Address line 2</th>
          <th>City</th>
          <th>Zip</th>
          <th>Country</th>
          <th>Actions</th>
        </thead>
        <tbody>
          <?php foreach ($this->addresses as $address) { ?>
          <tr>
            <td><?php echo $address->id ?></td>
            <td><?php echo $address->title ?></td>
            <td><?php echo $address->firstname ?></td>
            <td><?php echo $address->lastname ?></td>
            <td><?php echo $address->addressLine1 ?></td>
            <td><?php echo $address->addressLine2 ?></td>
            <td><?php echo $address->city ?></td>
            <td><?php echo $address->zip ?></td>
            <td><?php echo $address->country ?></td>
            <td>
              <button id="btn-link-<?php echo $address->id ?>" class="btn btn-primary" onclick="linkAddress(<?php echo $address->id ?>)"><i class="fa fa-link" aria-hidden="true"></i> <?php echo JText::_('COM_ALKASUBSCRIPTIONS_LINK'); ?></button>
            </td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
    <div class="modal-footer">
      <a class="btn" data-dismiss="modal" aria-hidden="true"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_CLOSE'); ?></a>
    </div>
    <input type="hidden" name="controller" value="addresses"/>
    <input type="hidden" name="option" value="com_alkasubscriptions"/>
    <input type="hidden" name="task"  class="task" value="link"/>
  <!-- </form> -->
</div>
