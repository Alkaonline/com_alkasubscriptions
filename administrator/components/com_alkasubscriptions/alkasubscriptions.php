<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_alkasubscriptions
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
require_once(JPATH_COMPONENT.'/controller.php');
require_once (JPATH_SITE . '/components/com_alkasubscriptions/helpers/alkasubscriptions.php');

$exports = JFolder::files(JPATH_COMPONENT_ADMINISTRATOR.'/exports/', $filter = '.', false,true);

foreach($exports as $file){
    if(time()-filemtime($file) > 20){
        JFile::delete($file);
    }
}
if($controller = JFactory::getApplication()->input->get('controller')) {
    $path = JPATH_COMPONENT.'/controllers/'.$controller.'.php';
    if (file_exists($path)) {
        require_once $path;
    } else {
        $controller = '';
    }
}

$classname    = 'AlkaSubscriptionsController'.$controller;
$controller   = new $classname();

$controller->execute( JFactory::getApplication()->input->get('task',null,null) );

// Redirect if set by the controller
$controller->redirect();
