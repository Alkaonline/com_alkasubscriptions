<?php
/**
 * @package	HikaShop for Joomla!
 * @version	2.6.4
 * @author	hikashop.com
 * @copyright	(C) 2010-2016 HIKARI SOFTWARE. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

class plgHikashopOrder_remind_abonnement extends JPlugin
{
	var $message = '';
	
	public function onBeforeRender(){
		if(JFactory::getApplication()->input->get('mailcorrect',null,null)=='1'){
			
			/*$usersToImport = array();
			if (($handle = fopen(JPATH_SITE.'/plugins/hikashop/order_remind_abonnement/subscriptions_CLEAN.csv', "r")) !== FALSE) {
			  while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
				  $key = $data[16];
				  if(!isset($usersToImport[$key])){
					  $usersToImport[$key]=$data;
				  }
				  
			  }
			  fclose($handle);
			}

			if(sizeof($usersToImport)){
				foreach($usersToImport as $user){
					


					$recipient = JFactory::getUser($user[16]);
					
					//$link = JUri::root().'index.php?option=com_hikashop&view=product&layout=show&product_id=1&Itemid=578';
					
					//$link		= $this-> _add_var_to_url('hika_u', urlencode($user->username),$link);
					//$link		= $this-> _add_var_to_url('hika_p', sha1($params_autolog->get('salt').$user->password),$link);
					$identity = (!$user[9] && !$user[10] && $user[0])?$user[0]:$user[9]." ".$user[10];
					$body   = 	'<img src="cid:logo_id" alt="logo"/><br/>'
					    . 		"<p>Bonjour ".$identity.",</p>

								<p>Nous automatisons notre boutique en ligne pour un meilleur service plus efficace.</p>

								<p>Voici vos identifiant et mot de passe :</p>
								
								<p>Identifiant : ".$user[16]."<br/>
								Mot de passe : ".$user[20]."</p>
								
								<p>Vous n’avez rien à faire aujourd’hui si ce n’est conserver ces informations.</p>
								
								<p>À l’échéance de votre abonnement à la revue Forêt.Nature, vous recevrez un e-mail vous invitant à vous réabonner via un lien qui vous connectera automatiquement sur votre profil.</p>
								
								<p>Votre identifiant et mot de passe seront par contre nécessaires pour vous connecter à votre compte afin d’acheter un ouvrage sur notre boutique en ligne .</p>
								
								<p>Tout achat ou réabonnement devra être réglé par un virement bancaire doté d’une communication structurée qui vous sera communiquée dans le mail de confirmation de commande.</p>
								
								<p>Merci pour votre confiance et à très bientôt sur notre librairie en ligne.</p>
								
								<p>A votre service,<br/>
								Forêt Wallonne asbl</p>
								
								<p>Rue Nanon, 98  |  5000 Namur  |  Belgique</br>
								T +32 (0)81 390 800  |  info@foretwallonne.be</br>
								www.foretwallonne.be</br>
								</p>";
					
					$mailer = JFactory::getMailer();
					$config = JFactory::getConfig();
					$sender = array( 
					    $config->get( 'mailfrom' ),
					    $config->get( 'fromname' ) 
					);
					$mailer->setSubject('votre compte utilisateur Forêt Wallonne asbl');
					$mailer->setSender($sender);
					$mailer->addRecipient($recipient->email);
					$mailer->addBCC('sebastienh@alkaonline.be');
					$mailer->isHTML(true);
					$mailer->Encoding = 'base64';
					$mailer->setBody($body);
					// Optionally add embedded image
					$mailer->AddEmbeddedImage( JPATH_SITE.'/images/BARRE-DE-LOGOS.jpg', 'logo_id', 'logo.jpg', 'base64', 'image/jpeg' );
					$send = $mailer->Send();
					
				}
			}
			
			//0 	: Société		(Facturation)
			//1 	: Prénom		(Facturation)
			//2 	: Nom			(Facturation)
			//3 	: adresse		(Facturation)
			//4 	: adresse 2		(Facturation)
			//5 	: ZIP			(Facturation)
			//6 	: Localité		(Facturation)
			//7 	: Pays			(Facturation)
			//8 	: Société 		(Livraison)
			//9 	: Prénom 		(Livraison)
			//10 	: Nom 			(Livraison)
			//11 	: adresse 		(Livraison)
			//12 	: adresse 2 	(Livraison)
			//13 	: ZIP 			(Livraison)
			//14 	: Localité 		(Livraison)
			//15 	: Pays 			(Livraison)
			//16 	: Email
			//17 	: Telephone	
			//18 	: TVA
			//19 	: Date de fin
			//20 	: pass
			
/*			$plugin = JPluginHelper::getPlugin('system', 'hikashopautolog');
			$params_autolog = new JRegistry($plugin->params);

			$today = new Datetime();
			$today->sub(new DateInterval('P1D'));
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			 
			$query->select($db->quoteName(array('#__hikashop_order.order_id','#__hikashop_order.order_status')));
			$query->from($db->quoteName('#__hikashop_order'));
			$query->where($db->quoteName('#__hikashop_order.order_invoice_id').' <= 134');
			$query->where($db->quoteName('#__hikashop_order.order_invoice_id').' > 60');
			$query->order($db->quoteName('#__hikashop_order.order_invoice_id'));
			$db->setQuery($query);
			$results = $db->loadObjectList();


			$config =& hikashop_config();

			if(sizeof($results)>0){
				foreach($results as $order){
					$orderObj 								= new stdClass();
					$orderObj->order_id 					= (int)$order->order_id;
					$orderObj->order_status 				= $config->get('order_confirmed_status', 'confirmed');
					$orderObj->history 						= new stdClass();
					$orderObj->history->history_notified 	= 1;
					$orderObj->history->history_reason 		= "MASS Send Mail";
					$orderClass 							= hikashop_get('class.order');
					$html 									= $orderClass->save($orderObj);

					
					$orderObj 								= new stdClass();
					$orderObj->order_id 					= (int)$order->order_id;
					$orderObj->order_status 				= $order->order_status;
					$orderObj->history 						= new stdClass();
					$orderObj->history->history_notified 	= 0;
					$orderObj->history->history_reason 		= "Restore to initial status";
					$orderClass 							= hikashop_get('class.order');
					$html 									= $orderClass->save($orderObj);

				}
			}
*/

			
/*
			$plugin = JPluginHelper::getPlugin('system', 'hikashopautolog');
			$params_autolog = new JRegistry($plugin->params);

					$user = JFactory::getUser(810);
					
					$link = JUri::root().'index.php?option=com_hikashop&view=product&layout=show&product_id=1&Itemid=578';
					
					$link		= $this-> _add_var_to_url('hika_u', urlencode($user->username),$link);
					$link		= $this-> _add_var_to_url('hika_p', sha1($params_autolog->get('salt').$user->password),$link);
					
					$body   = 	'<img src="cid:logo_id" alt="logo"/><br/>'
					    . 		"<p>Madame, Monsieur,</p>

								<p>Votre abonnement à Forêt.Nature est arrivé à échéance et nous n’avons pas encore reçu votre renouvellement.</p>
								
								<p>Pour ce faire, il vous suffit de cliquer sur ce lien et de sélectionner l’abonnement ad hoc.</p>
								 
								<p><a href='".JRoute::_($link)."'>".JRoute::_($link)."</a></p>
								
								<p>Il suffit ensuite d’effectuer le versement bancaire avec les renseignements que vous recevrez par e-mail.</p>

								<p>Seul le paiement par virement bancaire avec la communication structurée validera définitivement votre commande. Dès validation, vous recevrez une facture acquittée.</p>

								<p>Merci pour votre confiance et à votre service.</p>
								
								<p>Forêt Wallonne asbl</p>";
					
					$mailer = JFactory::getMailer();
					$config = JFactory::getConfig();
					$sender = array( 
					    $config->get( 'mailfrom' ),
					    $config->get( 'fromname' ) 
					);
					$mailer->setSubject('RAPPEL UNIQUE : Forêt.Nature réabonnement');
					$mailer->setSender($sender);
					$mailer->addRecipient($user->email);
					$mailer->isHTML(true);
					$mailer->Encoding = 'base64';
					$mailer->setBody($body);
					// Optionally add embedded image
					$mailer->AddEmbeddedImage( JPATH_SITE.'/images/BARRE-DE-LOGOS.jpg', 'logo_id', 'logo.jpg', 'base64', 'image/jpeg' );
					$send = $mailer->Send();
*/
			
					
/*
		$usersToImport = array();
		if (($handle = fopen(JPATH_SITE.'/plugins/hikashop/order_remind_abonnement/exportAbo2_PASS_corrected.csv', "r")) !== FALSE) {
		  while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
			  $usersToImport[]=$data;
		  }
		  fclose($handle);
		}
		
		//0 	: Société		(Facturation)
		//1 	: Prénom		(Facturation)
		//2 	: Nom			(Facturation)
		//3 	: adresse		(Facturation)
		//4 	: adresse 2		(Facturation)
		//5 	: ZIP			(Facturation)
		//6 	: Localité		(Facturation)
		//7 	: Pays			(Facturation)
		//8 	: Société 		(Livraison)
		//9 	: Prénom 		(Livraison)
		//10 	: Nom 			(Livraison)
		//11 	: adresse 		(Livraison)
		//12 	: adresse 2 	(Livraison)
		//13 	: ZIP 			(Livraison)
		//14 	: Localité 		(Livraison)
		//15 	: Pays 			(Livraison)
		//16 	: Email
		//17 	: Telephone	
		//18 	: TVA
		//19 	: Date de fin
		//20 	: pass
		if(sizeof($usersToImport)){
			foreach($usersToImport as $user){
				$body   = 	'<img src="cid:logo_id" alt="logo"/><br/>'
					    . 		"<p>Madame, Monsieur,</p>

								<p>Votre abonnement à la revue Forêt.Nature est arrivé à échéance.<br/>
								Espérons qu’elle vous a apporté autant de satisfaction à vous de la lire qu’à nous de la réaliser. </p>
								
								<p>Pour éviter de rater le prochain numéro, réabonnez-vous sans tarder.</p>
								
								<p>Pour ce faire, il vous suffit de cliquer sur ce lien :</p>
								 
								<p><a href='".JRoute::_($link)."'>".JRoute::_($link)."</a></p>
								
								<p>Il vous faudra alors juste sélectionner votre formule d’abonnement puis effectuer le versement bancaire avec la communication structurée que vous recevrez par e-mail.</p>
								
								<p>Dès votre payement réceptionné, vous recevrez une facture acquittée.</p>
								
								<p>Nous vous remercions vivement pour votre confiance et sommes à votre disposition pour toute question ou suggestion permettant d'améliorer votre revue Forêt.Nature.</p>
								
								<p>Forêt Wallonne asbl</p>";
					
				$mailer = JFactory::getMailer();
				$config = JFactory::getConfig();
				$sender = array( 
				    $config->get( 'mailfrom' ),
				    $config->get( 'fromname' ) 
				);
				$mailer->setSubject('Revue Forêt.Nature réabonnement');
				$mailer->setSender($sender);
				$mailer->addRecipient($user->email);
				$mailer->isHTML(true);
				$mailer->Encoding = 'base64';
				$mailer->setBody($body);
				// Optionally add embedded image
				$mailer->AddEmbeddedImage( JPATH_SITE.'/images/BARRE-DE-LOGOS.jpg', 'logo_id', 'logo.jpg', 'base64', 'image/jpeg' );
				//$send = $mailer->Send();
			}
		}
		
		echo "<textarea>";
		print_r ($usersToImport);
		echo "</textarea>";
*/
			
			
		}
	}


	public function onHikashopCronTrigger(&$messages){
		
			$plugin = JPluginHelper::getPlugin('system', 'hikashopautolog');
			$params_autolog = new JRegistry($plugin->params);

			$today = new Datetime();
			$today->sub(new DateInterval('P1D'));
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			 
			$query->select($db->quoteName(array('user_id')));
			$query->from($db->quoteName('#__akeebasubs_subscriptions'));
			$query->where($db->quoteName('#__akeebasubs_subscriptions.publish_down').' LIKE '.$db->quote($today->format('Y').'-'.$today->format('m').'-'.$today->format('d').'%'));
			
			$db->setQuery($query);
			$results = $db->loadColumn();
			
			if(sizeof($results)){
				foreach($results as $id_user){
					$user = JFactory::getUser($id_user);
					if(!in_array(14, $user->groups)){
					$link = JUri::root().'index.php?option=com_hikashop&view=product&layout=show&product_id=1&Itemid=578';
					
					$link		= $this-> _add_var_to_url('hika_u', urlencode($user->username),$link);
					$link		= $this-> _add_var_to_url('hika_p', sha1($params_autolog->get('salt').$user->password),$link);
					
					$body   = 	'<img src="cid:logo_id" alt="logo"/><br/>'
					    . 		"<p>Madame, Monsieur,</p>

								<p>Votre abonnement à Forêt.Nature est arrivé à échéance et nous n’avons pas encore reçu votre renouvellement.</p>
								
								<p>Pour ce faire, il vous suffit de cliquer sur ce lien et de sélectionner l’abonnement ad hoc.</p>
								 
								<p><a href='".JRoute::_($link)."'>".JRoute::_($link)."</a></p>
								
								<p>Il suffit ensuite d’effectuer le versement bancaire avec les renseignements que vous recevrez par e-mail.</p>

								<p>Seul le paiement par virement bancaire avec la communication structurée validera définitivement votre commande. Dès validation, vous recevrez une facture acquittée.</p>

								<p>Merci pour votre confiance et à votre service.</p>
								
								<p>Forêt Wallonne asbl</p>";
					
					$mailer = JFactory::getMailer();
					$config = JFactory::getConfig();
					$sender = array( 
					    $config->get( 'mailfrom' ),
					    $config->get( 'fromname' ) 
					);
					$mailer->setSubject('Revue Forêt.Nature réabonnement');
					$mailer->setSender($sender);
					$mailer->addRecipient($user->email);
					$mailer->isHTML(true);
					$mailer->Encoding = 'base64';
					$mailer->setBody($body);
					// Optionally add embedded image
					$mailer->AddEmbeddedImage( JPATH_SITE.'/images/BARRE-DE-LOGOS.jpg', 'logo_id', 'logo.jpg', 'base64', 'image/jpeg' );
					$send = $mailer->Send();
					}					
				}
			}
			
			$in15days = new Datetime();
			$in15days->sub(new DateInterval('P15D'));
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			 
			$query->select($db->quoteName(array('user_id')));
			$query->from($db->quoteName('#__akeebasubs_subscriptions'));
			$query->where($db->quoteName('#__akeebasubs_subscriptions.publish_down').' LIKE '.$db->quote($in15days->format('Y').'-'.$in15days->format('m').'-'.$in15days->format('d').'%'));
			$db->setQuery($query);
			$results = $db->loadColumn();
			
			if(sizeof($results)){
				foreach($results as $id_user){
					$user = JFactory::getUser($id_user);
					if(!in_array(14, $user->groups)){
						$link = JUri::root().'index.php?option=com_hikashop&view=product&layout=show&product_id=1&Itemid=578';
						
						$link		= $this-> _add_var_to_url('hika_u', urlencode($user->username),$link);
						$link		= $this-> _add_var_to_url('hika_p', sha1($params_autolog->get('salt').$user->password),$link);
						
						$body   = 	'<img src="cid:logo_id" alt="logo"/><br/>'
						    . 		"<p>Madame, Monsieur,</p>
						
									<p>Votre abonnement à la revue Forêt.Nature est arrivé à échéance.<br/>
									Espérons qu’elle vous a apporté autant de satisfaction à vous de la lire qu’à nous de la réaliser. </p>
									
									<p>Pour éviter de rater le prochain numéro, réabonnez-vous sans tarder.</p>
									
									<p>Pour ce faire, il vous suffit de cliquer sur ce lien :</p>
									 
									<p><a href='".JRoute::_($link)."'>".JRoute::_($link)."</a></p>
									
									<p>Il vous faudra alors juste sélectionner votre formule d’abonnement puis effectuer le versement bancaire avec la communication structurée que vous recevrez par e-mail.</p>
									
									<p>Dès votre payement réceptionné, vous recevrez une facture acquittée.</p>
									
									<p>Nous vous remercions vivement pour votre confiance et sommes à votre disposition pour toute question ou suggestion permettant d'améliorer votre revue Forêt.Nature.</p>
									
									<p>Forêt Wallonne asbl</p>";
						
						$mailer = JFactory::getMailer();
						$config = JFactory::getConfig();
						$sender = array( 
						    $config->get( 'mailfrom' ),
						    $config->get( 'fromname' ) 
						);
						$mailer->setSubject('Revue Forêt.Nature réabonnement');
						$mailer->setSender($sender);
						$mailer->addRecipient($user->email);
						$mailer->isHTML(true);
						$mailer->Encoding = 'base64';
						$mailer->setBody($body);
						// Optionally add embedded image
						$mailer->AddEmbeddedImage( JPATH_SITE.'/images/BARRE-DE-LOGOS.jpg', 'logo_id', 'logo.jpg', 'base64', 'image/jpeg' );
						$send = $mailer->Send();
					}
				}
			}
		
	}
	
	
	
	
	
	function _add_var_to_url($variable_name,$variable_value,$url_string){

		if(strpos($url_string,"?")){
			$start_pos = strpos($url_string,"?");
			$url_vars_strings = substr($url_string,$start_pos+1);
			$names_and_values = explode("&",$url_vars_strings);
			$url_string = substr($url_string,0,$start_pos);
			foreach($names_and_values as $value){
				list($var_name,$var_value)=explode("=",$value);
				if($var_name != $variable_name){
					if(strpos($url_string,"?")===false){
						$url_string.= "?";
					} else {
						$url_string.= "&";
					}
					$url_string.= $var_name."=".$var_value;
				}
			}
		}
		// add variable name and variable value
		if(strpos($url_string,"?")===false){
			$url_string .= "?".$variable_name."=".$variable_value;
		} else {
			$url_string .= "&".$variable_name."=".$variable_value;
		}
		return $url_string;
	}
}
