<?php
/**
 * @package	HikaShop for Joomla!
 * @version	2.6.4
 * @author	hikashop.com
 * @copyright	(C) 2010-2016 HIKARI SOFTWARE. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

class plgSystemTransportexporter extends JPlugin
{
	function onBeforeRender(){
		$app 	= JFactory::getApplication();
		$option = $app->input->get('option',null,null);
		$view 	= $app->input->get('view',null,null);
		$layout = $app->input->get('layout',null,null);
		$doc 	= JFactory::getDocument();

		$ajxParams = JFactory::getApplication()->input->get('ajxparams',null,null);
		if($ajxParams['action']){
			$return = '';
			switch($ajxParams['action']){
				case 'getTrExTable': $return = $this->_getTrExTable(); break;
				case 'setTrEx': $return = json_encode($this->_setTrEx($ajxParams['data'])); break;
			}
			echo $return;
			die();
		}


		if($app->isAdmin() && $option=='com_hikashop' && $view=='order' && $layout=='listing'){
			$doc->addScriptDeclaration('
				var HIKA_TREX_TOOLBAR_BTN="Export Transporteur";
				var PLG_SYSTEM_HIKASHOPTREX ="Export Transporteur";
				var HIKA_TREX_APPLY_BTN="Marquez les commandes cochées comme expédiées";
				var HIKA_TREX_CONFIRM_TITLE="";
				var HIKA_TREX_CONFIRM_CONTENT="";
			');

			$doc->addScript(JUri::root(true).'/media/plg_system_transportexporter/js/transportexporter.js');
			$doc->addStyleDeclaration('#modal-Hika-trex .modal-body{
								max-height: 600px;
								overflow-y:auto;
							}');
		}
	}

	function onAjaxTransportexporter(){
		$ajxParams = JFactory::getApplication()->input->get('ajxparams',null,null);
		$return = '';
		switch($ajxParams['action']){
			case 'getTrExTable': $return = $this->_getTrExTable(); break;
			case 'setTrEx': $return = $this->_setTrEx($ajxParams['data']); break;
		}
		return $return;
	}

	function _updateLastExportDate(){
		if(!@include_once(rtrim(JPATH_ADMINISTRATOR,DS).DS.'components'.DS.'com_hikashop'.DS.'helpers'.DS.'helper.php')){ return false; }
		$pluginsClass = hikashop_get('class.plugins');
		$plugin = $pluginsClass->getByName('system','transportexporter');
		$plugin->params['last_export']=time();
		$pluginsClass->save($plugin);
		$return = true;
	}

	function _setTrEx($data){
		if(sizeof($data)){
			if(!@include_once(rtrim(JPATH_ADMINISTRATOR,DS).DS.'components'.DS.'com_hikashop'.DS.'helpers'.DS.'helper.php')){ return false; }
			$orderClass = hikashop_get('class.order');
			$status = 'shipped';
			foreach($data as $order){
				$update = new stdClass();
				$update->order_id = $order;
				$update->order_status = $status;
				$update->history = new stdClass();
				$update->history->history_notified = 0;
				$orderClass->save($update);
			}
		}

		return $data;
	}

	function _getTrExTable(){
		if(!@include_once(rtrim(JPATH_ADMINISTRATOR,DS).DS.'components'.DS.'com_hikashop'.DS.'helpers'.DS.'helper.php')){ return false; }
		$pluginsClass = hikashop_get('class.plugins');
		$plugin = $pluginsClass->getByName('system','transportexporter');

		$config =& hikashop_config();
		$status = $config->get('order_confirmed_status');

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query->select('DISTINCT '.$db->quoteName('#__hikashop_history.history_order_id'));
		$query->from($db->quoteName('#__hikashop_history'));
		$query->where($db->quoteName('#__hikashop_history.history_created') . ' >= '. ((int)$plugin->params['last_export']));
		$query->where($db->quoteName('#__hikashop_history.history_new_status') . ' = '. $db->quote($status));
		$query->join('INNER',$db->quoteName('#__hikashop_order').'ON ('.$db->quoteName('#__hikashop_history.history_order_id').'='.$db->quoteName('#__hikashop_order.order_id').')');
		$query->where($db->quoteName('#__hikashop_order.order_status') . ' = '. $db->quote($status));

		$db->setQuery($query);

		$results = $db->loadColumn();
		$return = array();
		if(sizeof($results)){
			$orderClass = hikashop_get('class.order');
			$colnumbers = 0;
			foreach($results as $order_id){
				$order = $orderClass->loadFullOrder($order_id, true, false);

				$returnOrder = array();
				$returnOrder[] = $order->order_id;
				$returnOrder[] = $order->order_number;
                $returnOrder[] = $order->shipping_address->address_company;
				$returnOrder[] = $order->shipping_address->address_firstname;
				$returnOrder[] = $order->shipping_address->address_lastname;
				$returnOrder[] = $order->shipping_address->address_street;
				$returnOrder[] = $order->shipping_address->address_post_code;
				$returnOrder[] = $order->shipping_address->address_city;
                $returnOrder[] = $order->shipping_address->address_country;
				if(sizeof($order->products)){
					$c = 0;
					foreach($order->products as $product){
						$returnOrder[] = $product->order_product_quantity;
						$returnOrder[] = $product->order_product_code;
						$c++;
						if($colnumbers<$c){ $colnumbers=$c; }
					}
				}
				$return[]=$returnOrder;
			}
		}

		$html  = '<thead>';
		$html .= 	'<tr>';
		$html .= 		'<th data-checkbox="true" >Expédiée</th>';
		$html .= 		'<th data-sortable="true" >ID</th>';
		$html .= 		'<th data-sortable="true" >N. de commande</th>';
		$html .= 		'<th data-sortable="true" >Société</th>';
		$html .= 		'<th data-sortable="true" >Prénom</th>';
		$html .= 		'<th data-sortable="true" >Nom</th>';
		$html .= 		'<th data-sortable="true" >Adresse</th>';
		$html .= 		'<th data-sortable="true" >CP</th>';
		$html .= 		'<th data-sortable="true" >Commune</th>';
        $html .= 		'<th data-sortable="true" >Pays</th>';
		for($i=1;$i<$colnumbers+1;$i++){
			$html .= 	'<th data-sortable="true" >Q'.$i.'</th>';
			$html .= 	'<th data-sortable="true" >Product'.$i.'</th>';
		}
		$html .= 	'</tr>';
		$html .='</thead>';
		$html .='<tbody>';
		if(sizeof($return)){
			foreach($return as $order){
				$html .= 	'<tr data-id="'.$order[0].'">';
				$html .= 		'<td></td>';
				$html .= 		'<td>'.$order[0].'</td>';
				$html .= 		'<td>'.$order[1].'</td>';
				$html .= 		'<td>'.$order[2].'</td>';
				$html .= 		'<td>'.$order[3].'</td>';
				$html .= 		'<td>'.$order[4].'</td>';
				$html .= 		'<td>'.$order[5].'</td>';
				$html .= 		'<td>'.$order[6].'</td>';
				$html .= 		'<td>'.$order[7].'</td>';
                $html .= 		'<td>'.$order[8].'</td>';
				for($i=9;$i<($colnumbers*2)+9;$i++){
					$html .= 	'<td>'.$order[$i].'</td>';
				}
				$html .= 	'</tr>';
			}
		}


		$html .='</tbody>';
		return $html;
	}


}
