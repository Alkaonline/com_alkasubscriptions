<?php
/**
 * @package	HikaShop for Joomla!
 * @version	2.6.4
 * @author	alkaonline.be
 * @copyright	(C) 2010-2016 Alka. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php
class plgHikashoppaymentBanktransfercomm extends hikashopPaymentPlugin {
	var $name = 'banktransfercomm';
	var $multiple = true;
	var $pluginConfig = array(
		'order_status' => array('ORDER_STATUS', 'orderstatus', 'verified'),
		'status_notif_email' => array('ORDER_STATUS_NOTIFICATION', 'boolean','0'),
		'information' => array('BANK_ACCOUNT_INFORMATION', 'big-textarea'),
		'return_url' => array('RETURN_URL', 'input'),
	);

	function onBeforeOrderCreate(&$order,&$do){
		parent::onBeforeOrderCreate($order,$do);

		$user_id 						= $order->order_user_id;

		$options 						= (object)$order->paymentOptions;
		$options->communication 		= $this->_generate_structured_communication();
		$order->order_payment_params 	= $options;
	}


	function onAfterOrderConfirm(&$order,&$methods,$method_id) {
		parent::onAfterOrderConfirm($order,$methods,$method_id);
		if($order->order_status != $this->payment_params->order_status)
			$this->modifyOrder($order->order_id, $this->payment_params->order_status, (bool)@$this->payment_params->status_notif_email, false);

		$this->removeCart = true;

		$this->information = $this->payment_params->information;
		if(preg_match('#^[a-z0-9_]*$#i',$this->information)){
			$this->information = JText::_($this->information);
		}
		$currencyClass = hikashop_get('class.currency');
		$this->amount = $currencyClass->format($order->order_full_price,$order->order_currency_id);
		$this->order_number = $order->order_number;
		$this->bankcommunication = $order->order_payment_params->communication;
		$this->return_url =& $this->payment_params->return_url;
		return $this->showPage('end');

	}

	function getPaymentDefaultValues(&$element) {
		$element->payment_name='Bank transfer + Structured Communication';
		$element->payment_description='You can pay by sending us a bank transfer and receive a unique communication number.';
		$element->payment_images='';

		$element->payment_params->information='Account owner: XXXXX<br/>
<br/>
Owner address:<br/>
<br/>
XX XXXX XXXXXX<br/>
<br/>
XXXXX XXXXXXXX<br/>
<br/>
IBAN International Bank Account Number:<br/>
<br/>
XXXX XXXX XXXX XXXX XXXX XXXX XXX<br/>
<br/>
BIC swift Bank Identification Code:<br/>
XXXXXXXXXXXXXX<br/>
<br/>
Structured communication : <strong>{bankcommunication}</strong><br/>
<br/>
Bank name: XXXXXXXXXXX<br/>
<br/>
Bank address:<br/>
<br/>
XX XXXX XXXXXX<br/>
<br/>
XXXXX XXXXXXXX';
		$element->payment_params->order_status='created';
	}

	function _generate_structured_communication() {
        $databaseName=JFactory::getConfig()->get('db');
		$Idquery = "SELECT Auto_increment FROM information_schema.tables WHERE table_name='".JFactory::getApplication()->get('dbprefix')."hikashop_order' AND table_schema = '".$databaseName."'";
		$db = JFactory::getDbo();
		$db->setQuery($Idquery);
		$id = $db->loadResult();
    
		$base = (float) $id;

		$control = $base - floor($base / 97) * 97;
		if ($control == 0) {
		  $control = 97;
		}

		$base_s = (string) $base;
		$control_s = (string) $control;

		if ($control < 10) {
		  $control_s = "0" . $control_s;
		}

		$base_len = strlen($base_s);
		$count = 10 - $base_len;

		for ($i=0; $i < $count; $i++) {
		  $base_s = "0" . $base_s;
		}

		$com = $base_s . $control_s;

		return substr($com, 0, 3) . "/" . substr($com, 3, 4) . "/" . substr($com, 7, 5);
	}
}
