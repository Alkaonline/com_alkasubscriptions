<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Alkaintranet
 * @author     Alka <developpement@alkaonline.be>
 * @copyright  2016 Alka
 * @license    GNU General Public License version 2 ou version ultérieure ; Voir LICENSE.txt
 */
defined('_JEXEC') or die;

class AlkaSubscriptionsModelAddresses extends JModelLegacy
{
  static public function getAddresses()
  {
      $currentUserID = JFactory::getUser()->id;

      $db     = JFactory::getDBO();
      $query  = $db->getQuery(true);
      $query->select(
          array(
              $db->quoteName('#__alkasubscriptions_address').'.*',
          )
      );
      $query->from($db->quoteName('#__alkasubscriptions_address'));
      $query->where($db->quoteName('idUser') . ' = ' . $currentUserID . ' AND ' . $db->quoteName('enabled') . ' = 1');
      $db->setQuery((string) $query);
      $items = $db->loadObjectList();
      return $items;
  }
}
