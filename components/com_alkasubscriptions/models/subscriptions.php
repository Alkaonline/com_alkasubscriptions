<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Alkaintranet
 * @author     Alka <developpement@alkaonline.be>
 * @copyright  2016 Alka
 * @license    GNU General Public License version 2 ou version ultérieure ; Voir LICENSE.txt
 */
defined('_JEXEC') or die;

class AlkaSubscriptionsModelSubscriptions extends JModelLegacy
{
    static public function getSubscriptions()
    {
        $currentUserID = JFactory::getUser()->id;

        $db     = JFactory::getDBO();
        $query  = $db->getQuery(true);

        $query->select(
            array(
                //$db->quoteName('#__alkasubscriptions_subscription').'.*', $db->quoteName('#__alkasubscriptions_address').'.*'
                's.id AS idSub',
                $db->quoteName('r.productName','name'),
                $db->quoteName('s.name','initialName'),
                $db->quoteName('r.productId','idProduct'),
                $db->quoteName('p.product_alkasubscription_can_be_renew_with_main','mainProductToRenew'),
                's.name',
                's.startDate',
                's.endDate',
                's.paid',
                's.enabled',
                's.idUser',
                's.idProduct',
                's.startIssueNumber',
                's.endIssueNumber',
                's.productOptions',
                's.subscriptionType',
                'a.firstname',
                'a.lastname',
                'a.id AS idAdd',
                'a.title',
                'a.addressLine1',
                'a.addressLine2',
                'a.zip',
                'a.city',
            )
        );

        $query->from($db->quoteName('#__alkasubscriptions_subscription', 's'));
        $query->join('LEFT', $db->quoteName('#__alkasubscriptions_address', 'a')
          . ' ON (' .
          $db->quoteName('a.id')
          . ' = ' .
          $db->quoteName('s.idAddress') . ')');

        $query->join('LEFT', $db->quoteName('#__alkasubscriptions_renew_history', 'r')
        . ' ON (r.subscription_id = s.id AND r.start <= NOW() AND r.end > NOW())');
        $query->join('LEFT', $db->quoteName('#__hikashop_product', 'p')
          . ' ON (' .
          $db->quoteName('p.product_id')
          . ' = ' .
          $db->quoteName('s.idProduct') . ')');
        $query->where($db->quoteName('s.idUser') . ' = ' . $currentUserID);

        $query->order($db->quoteName('s.id') . ' DESC');
        $db->setQuery((string) $query);
        $items = $db->loadObjectList();
        return $items;
    }

    static public function getAddresses()
    {
        $currentUserID = JFactory::getUser()->id;

        $db     = JFactory::getDBO();
        $query  = $db->getQuery(true);
        $query->select(
            array(
                $db->quoteName('#__alkasubscriptions_address').'.*'
            )
        );
        $query->from($db->quoteName('#__alkasubscriptions_address'));
        $query->where($db->quoteName('idUser') . ' = ' . $currentUserID . ' AND ' . $db->quoteName('enabled') . ' = 1');
        $db->setQuery((string) $query);
        $items = $db->loadObjectList();
        return $items;
    }

    static public function getProductsToExtend(){
        $db     = JFactory::getDBO();
        $query  = $db->getQuery(true);
        $query->select(
            array(
                $db->quoteName('#__hikashop_product').'.*'
            )
        );
        $query->from($db->quoteName('#__hikashop_product'));
        $query->where($db->quoteName('product_alkasubscription_type') . ' IS NOT NULL');
        $query->where($db->quoteName('product_published') . ' > 0');
        $db->setQuery($query);
        $items = $db->loadObjectList('product_id');
        $return = array();
        foreach($items as $item){
            if($item->product_alkasubscription_can_be_renew_with){
                $renewWith = explode(',', $item->product_alkasubscription_can_be_renew_with);
                foreach($renewWith as $renew){
                    $return[$item->product_id][]=$items[$renew];
                }
            }

        }
        return $return;
    }

}
