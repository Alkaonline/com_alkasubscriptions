<?php
/**
 * @version    CVS: 1.0.0
 * @package    com_alkasubscriptions
 * @author     Alka <developpement@alkaonline.be>
 * @copyright  2017 Alka
 * @license    GNU General Public License version 2 ou version ultérieure ; Voir LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

require_once(JPATH_COMPONENT.'/controller.php');
require_once (JPATH_COMPONENT . '/helpers/alkasubscriptions.php');

if($controller = JFactory::getApplication()->input->get('controller')) {
    $path = JPATH_COMPONENT.'/controllers/'.$controller.'.php';
    if (file_exists($path)) {
        require_once $path;
    } else {
        $controller = '';
    }
}

$classname    = 'AlkaSubscriptionsController'.$controller;
$controller   = new $classname();

$controller->execute( JFactory::getApplication()->input->get('task',null,null) );

// Redirect if set by the controller
$controller->redirect();
