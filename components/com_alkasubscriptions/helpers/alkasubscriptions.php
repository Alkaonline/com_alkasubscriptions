<?php

/**
 * @version    CVS: 1.0.0
 * @package    com_alkasubscriptions
 * @author     Alka <developpement@alkaonline.be>
 * @copyright  2017 Alka
 * @license    GNU General Public License version 2 ou version ultérieure ; Voir LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Class AlkaSubscriptionsFrontendHelper
 *
 * @since  1.0
 */
class AlkaSubscriptionsHelper
{

	public static function setSubscription($id, $name, $startDate, $endDate, $renewDate, $forever, $enabled, $idUser, $idProduct, $idOrder, $idAddress, $productOptions, $subscriptionType, $startIssueNumber, $endIssueNumber,$paid=0)
	{

		$productOptions = (@unserialize($productOptions))?$productOptions:serialize($productOptions);
        // Get a db connection
        $db = JFactory::getDbo();

		// Create a new query object
		$query = $db->getQuery(true);

		$nowDate = new Datetime();
		$currentUserID = JFactory::getUser()->id;

        $deadLines = AlkaSubscriptionsHelper::getDeadlines('FO', 3, $newDateTime);

		if($id !== null && $id !== ''){
			// Update query

			// Fields to update
			$fields = array(
					$db->quoteName('name') . ' = ' . $db->quote($name),
					$db->quoteName('startDate') . ' = ' . $db->quote($startDate),
					$db->quoteName('endDate') . ' = ' . $db->quote($endDate),
					$db->quoteName('renewDate') . ' = ' . $db->quote($renewDate),
					$db->quoteName('forever') . ' = ' . $forever,
			        $db->quoteName('modifiedDate') . ' = ' . $db->quote($nowDate->format('Y-m-d H:i:s')),
			        $db->quoteName('modifiedBy') . ' = ' . $currentUserID,
					$db->quoteName('enabled') . ' = ' . $enabled,
					$db->quoteName('idUser') . ' = ' . $idUser,
					$db->quoteName('idProduct') . ' = ' . $idProduct,
					//$db->quoteName('idOrder') . ' = ' . $idOrder,
					$db->quoteName('idAddress') . ' = ' . $idAddress,
					$db->quoteName('productOptions') . ' = ' . $db->quote($productOptions),
                    $db->quoteName('paid') . ' = ' . $db->quote($paid),
                    $db->quoteName('subscriptionType') . ' = ' . $subscriptionType,
                    $db->quoteName('startIssueNumber') . ' = ' . $startIssueNumber,
                    $db->quoteName('endIssueNumber') . ' = ' . $endIssueNumber

			);

			// Conditions for which records should be updated
			$conditions = array(
			    $db->quoteName('id') . ' = ' . $id
			);

			$query->update($db->quoteName('#__alkasubscriptions_subscription'))->set($fields)->where($conditions);

			$db->setQuery($query);

			$result = $db->execute();

			if($result){
                AlkaSubscriptionsHelper::addLinkOrderSubscription($idOrder,$id);
				return $id;
			} else {
				return false;
			}

		} else {
			// Insert query

	    // Insert columns
	    $columns = array(
				'name',
				'startDate',
				'endDate',
				'renewDate',
				'forever',
				'createdDate',
				'createdBy',
				'enabled',
				'idUser',
				'idProduct',
				//'idOrder',
				'idAddress',
				'productOptions',
                'paid',
                'subscriptionType',
                'startIssueNumber',
                'endIssueNumber'
			);

	    // Insert values
	    $values = array(
				$db->quote($name),
				$db->quote($startDate),
				$db->quote($endDate),
				$db->quote($renewDate),
				$forever,
				$db->quote($nowDate->format('Y-m-d H:i:s')),
				$currentUserID,
				$enabled,
				$idUser,
				$idProduct,
				//$idOrder,
				$idAddress,
				$db->quote($productOptions),
                $db->quote($paid),
                $db->quote($subscriptionType),
                $db->quote($startIssueNumber),
                $db->quote($endIssueNumber)
			);

	    // Prepare the insert query
	    $query
	        ->insert($db->quoteName('#__alkasubscriptions_subscription'))
	        ->columns($db->quoteName($columns))
	        ->values(implode(',', $values));

	    // Set the query using our newly populated query object and execute it
	    $db->setQuery($query);
	    $result = $db->execute();

			if($result){
                $subId = $db->insertid();
                AlkaSubscriptionsHelper::addLinkOrderSubscription($idOrder,$subId);
				return $subId;
			} else {
				return false;
			}
		}

	}

    public static function getTimeLine($subId,$html = false){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query->select("*");
        $query->from($db->quoteName('#__alkasubscriptions_subscription'));
        $query->where($db->quoteName('id') . ' = '. $db->quote($subId));
        $db->setQuery($query);
        $subscription = $db->loadObject();

        $query = $db->getQuery(true);

        $query->select("issue, date");
        $query->from($db->quoteName('#__alkasubscriptions_deadline'));
        $query->where($db->quoteName('subscription') . ' = '. $db->quote($subscription->subscriptionType));
        $query->where($db->quoteName('issue') . ' >= '. $db->quote($subscription->startIssueNumber));
        $query->where($db->quoteName('issue') . ' <= '. $db->quote($subscription->endIssueNumber));

        $db->setQuery($query);
        $deadlines = $db->loadObjectList();

        foreach($deadlines as &$deadline){
            $deadline->date = new DateTime($deadline->date);
        }
        $return = $deadlines;
        if($html){
            $html = '<table class="table" cellpadding="" cellspacing="">';
            $html .= '<thead>';
            $html .= '<tr>';
                $html.= '<th>Numéro</th>';
                $html.= '<th>Date d\'envoi</th>';
            $html .= '</tr>';
            $html .= '<thead>';
            $html .= '<tbody>';
            foreach($deadlines as $deadline){
                $html .= '<tr>';
                    $html.= '<td>'.$deadline->issue.'</td>';
                    $html.= '<td>'.$deadline->date->format('d/m/Y').'</td>';
                $html .= '</tr>';
            }
            $html .= '</tbody>';
            $html .= '</table>';
            $return = $html;
        }
        return $return;
    }

    public function getDeadlines($subType, int $issueQtty, $date = false){
        if(!$date){
            $date = new DateTime();
        }
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query->select("*");
        $query->from($db->quoteName('#__alkasubscriptions_deadline'));
        $query->where($db->quoteName('subscription') . ' = '. $db->quote($subType));
        $query->where($db->quoteName('date') . ' > ' . $db->quote($date->format('Y-m-d')));
        $query->setLimit($issueQtty);
        $db->setQuery($query);
        return $db->loadObjectList();
    }

    public function getDeadlineDateByIssue($issueNumber,$type){

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query->select("date");
        $query->from($db->quoteName('#__alkasubscriptions_deadline'));
        $query->where($db->quoteName('subscription') . ' = '. $db->quote($type));
        $query->where($db->quoteName('issue') . ' = ' . $db->quote($issueNumber));
        $query->setLimit($issueQtty);
        $db->setQuery($query);
        return $db->loadResult();
    }

    public static function getSubscriptionNameDynamic($subscription, $date = null){
      if(!$date){
        $date = new DateTime();
      }
      $id = $subscription->id ? $subscription->id:$subscription->idSub;

      $db = JFactory::getDbo();
      $query = $db->getQuery(true);
      $query->select("#__alkasubscriptions_renew_history.*");
      $query->from($db->quoteName('#__alkasubscriptions_renew_history'));
      $query->where($db->quoteName('#__alkasubscriptions_renew_history.subscription_id') . ' = '. $db->quote($id));
      $query->where($db->quoteName('#__alkasubscriptions_renew_history.start') . ' <= '. $db->quote($date->format('Y-m-d H:i:s')));
      $query->where($db->quoteName('#__alkasubscriptions_renew_history.end') . ' > '. $db->quote($date->format('Y-m-d H:i:s')));
      $query->setLimit('1');

      $db->setQuery($query);
      $subscriptionDetails = $db->loadObject();

      if($subscriptionDetails){
        $subscription->name = $subscriptionDetails->productName;
        $subscription->productCode = $subscriptionDetails->code;
      }
      return $subscription;
    }

    public static function getRenewOrdersBySubscription($subscription,$getInitial = false,$future = false){

      $id = $subscription->id ? $subscription->id:$subscription->idSub;
      $db = JFactory::getDbo();
      $query = $db->getQuery(true);
      $query->select("#__alkasubscriptions_renew_history.*, #__hikashop_order.*,'renew' as orderType");
      $query->from($db->quoteName('#__alkasubscriptions_renew_history'));
      $query->join('LEFT', $db->quoteName('#__hikashop_order').' ON ('.$db->quoteName('#__hikashop_order.order_id').' = '.$db->quoteName('#__alkasubscriptions_renew_history.orderId').')');
      $query->where($db->quoteName('#__alkasubscriptions_renew_history.subscription_id') . ' = '. $db->quote($id));
      if($future){
        $query->where($db->quoteName('#__alkasubscriptions_renew_history.start') . ' > NOW()');
      }
      $query->order('#__alkasubscriptions_renew_history.start ASC');
      $db->setQuery($query);
      $renews = $db->loadObjectList('order_id');

      if($getInitial && $future == false){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select("#__hikashop_order.*,'initial' as orderType");
        $query->from($db->quoteName('#__hikashop_order'));
        $query->join('INNER', $db->quoteName('#__alkasubscriptions_subscription_order').' ON ('.$db->quoteName('#__hikashop_order.order_id').' = '.$db->quoteName('#__alkasubscriptions_subscription_order.id_order').')');
        $query->where($db->quoteName('#__alkasubscriptions_subscription_order.id_subscription') . ' = '. $db->quote($id));
        $db->setQuery($query);
        $initials = $db->loadObjectList('order_id');
        $orders = array();
        if(sizeof($initials) && sizeof($renews)){
          $initial = false;
          foreach($renews as $order_id=>$renew){
            if(isset($initials[$order_id])){
              $renew->orderType = 'initial';
              $initial = true;
            }
            $orders[] = $renew;
          }
          if(!$initial){
            $orders = array_merge($initials,$renews);
          }
        }else{
          $orders = $initials;
        }
      }else if($future){
        array_splice($renews,0, 1);
        $orders = $renews;
      }else{
        $orders = $renews;

      }
      return $orders;
    }

    public function getSubscriptionsByOrder($idOrder){
        $query->select("s.*");
        $query->from($db->quoteName('#__alkasubscriptions_subscription_order'));
        $query->join('LEFT', $db->quoteName('#__alkasubscriptions_subscription','s').' ON ('.$db->quoteName('#__alkasubscriptions_subscription.id').' = '.$db->quoteName('#__alkasubscriptions_subscription_order.id_subscription').')');
        $query->where($db->quoteName('#__alkasubscriptions_subscription_order.id_order') . ' = '. $db->quote($idOrder));
        $db->setQuery($query);
        $subscriptions = $db->loadObjectList();

        return $subscriptions;
    }

    public function addLinkOrderSubscription($idOrder,$idSubscription){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query->select("*");
        $query->from($db->quoteName('#__alkasubscriptions_subscription_order'));
        $query->where($db->quoteName('id_order') . ' = '. $db->quote($idOrder));
        $query->where($db->quoteName('id_subscription') . ' = '. $db->quote($idSubscription));
        $db->setQuery($query);
        $subOrderLink = $db->loadObjectList();

        if(!count($subOrderLink)){
            $link = new StdClass();
            $link->id_subscription  = $idSubscription;
            $link->id_order         = $idOrder;
            $return = JFactory::getDbo()->insertObject('#__alkasubscriptions_subscription_order', $link);
        }else{
            $return = $subOrderLink;
        }

        return $return;
    }

	public static function setAddress($id, $title, $firstname, $lastname, $company, $addressLine1, $addressLine2, $city, $zip, $country, $enabled, $note, $hikaAddressId){

		// Get a db connection
    $db = JFactory::getDbo();

		// Create a new query object
		$query = $db->getQuery(true);

		$nowDate = new Datetime();
		$currentUserID = JFactory::getUser()->id;
		if(empty($hikaAddressId)){
			$hikaAddressId = 'null';
		}

		if($id){
			// Update query

			// Fields to update
			$fields = array(
					$db->quoteName('title') . ' = ' . $db->quote($title),
					$db->quoteName('firstname') . ' = ' . $db->quote($firstname),
					$db->quoteName('lastname') . ' = ' . $db->quote($lastname),
					$db->quoteName('company') . ' = ' . $db->quote($company),
					$db->quoteName('addressLine1') . ' = ' . $db->quote($addressLine1),
					$db->quoteName('addressLine2') . ' = ' . $db->quote($addressLine2),
					$db->quoteName('city') . ' = ' . $db->quote($city),
					$db->quoteName('zip') . ' = ' . $db->quote($zip),
					$db->quoteName('country') . ' = ' . $db->quote($country),
			    $db->quoteName('modifiedDate') . ' = ' . $db->quote($nowDate->format('Y-m-d H:i:s')),
			    $db->quoteName('modifiedBy') . ' = ' . $currentUserID,
					$db->quoteName('enabled') . ' = ' . $enabled,
					$db->quoteName('note') . ' = ' . $db->quote($note),
					$db->quoteName('hikaAddressId') . ' = ' . $hikaAddressId,
					$db->quoteName('idUser') . ' = ' . $currentUserID
			);

			// Conditions for which records should be updated
			$conditions = array(
			    $db->quoteName('id') . ' = ' . $id
			);

			$query->update($db->quoteName('#__alkasubscriptions_address'))->set($fields)->where($conditions);

			$db->setQuery($query);

			$result = $db->execute();

			if($result){
				return $id;
			} else {
				return false;
			}

		} else {
			// Insert query

	    // Insert columns
	    $columns = array(
				'title',
				'firstname',
				'lastname',
				'company',
				'addressLine1',
				'addressLine2',
				'city',
				'zip',
				'country',
				'createdDate',
				'createdBy',
				'enabled',
				'note',
				'hikaAddressId',
				'idUser');

	    // Insert values
	    $values = array(
				$db->quote($title),
				$db->quote($firstname),
				$db->quote($lastname),
				$db->quote($company),
				$db->quote($addressLine1),
				$db->quote($addressLine2),
				$db->quote($city),
				$db->quote($zip),
				$db->quote($country),
				$db->quote($nowDate->format('Y-m-d H:i:s')),
				$currentUserID,
				$enabled,
			  $db->quote($note),
				$hikaAddressId,
				$currentUserID);

	    // Prepare the insert query
	    $query
	        ->insert($db->quoteName('#__alkasubscriptions_address'))
	        ->columns($db->quoteName($columns))
	        ->values(implode(',', $values));

	    // Set the query using our newly populated query object and execute it
	    $db->setQuery($query);

			//die($query);

	    $result = $db->execute();

			if($result){
				return $db->insertid();
			} else {
				return false;
			}
		}

	}

	public static function setGroups($subscriptionId = null, $orderId = null, $forceParity=null){
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$params 	= JComponentHelper::getParams('com_alkasubscriptions');
			$query->select(array(
					$db->quoteName('#__alkasubscriptions_subscription.id').' AS subscriptionId',
					$db->quoteName('#__alkasubscriptions_subscription.idUser'),
					($params->get('mode')=='duration') ? $db->quoteName('#__alkasubscriptions_subscription.endDate') : $db->quoteName('#__alkasubscriptions_deadline.date').' AS endDate',
					$db->quoteName('#__alkasubscriptions_subscription.enabled'),
					$db->quoteName('#__alkasubscriptions_subscription.paid'),
					$db->quoteName('#__alkasubscriptions_subscription.forever'),
					($params->get('mode')=='duration') ? $db->quoteName('#__alkasubscriptions_subscription.endDate').' < NOW()    AS expired' : $db->quoteName('#__alkasubscriptions_deadline.date').' < NOW()    AS expired',
					$db->quoteName('#__hikashop_product.product_alkasubscription_groups_in_on_leave'),
					$db->quoteName('#__hikashop_product.product_alkasubscription_groups_out_on_leave'),
					$db->quoteName('#__hikashop_product.product_alkasubscription_groups_in_on_enter'),
					$db->quoteName('#__hikashop_product.product_alkasubscription_groups_out_on_enter'),
					'(SELECT GROUP_CONCAT('.$db->quoteName('#__user_usergroup_map.group_id').') AS userActualGroups  FROM '.$db->quoteName('#__user_usergroup_map').' WHERE '.$db->quoteName("#__user_usergroup_map.user_id").'= '.$db->quoteName('#__alkasubscriptions_subscription.idUser').' ) AS userActualGroups '
			));
			$query->from($db->quoteName('#__alkasubscriptions_subscription'));
			$query->join('INNER',$db->quoteName('#__hikashop_product') . ' ON ('.$db->quoteName("#__hikashop_product.product_id").' = '.$db->quoteName("#__alkasubscriptions_subscription.idProduct").') ');
			if($params->get('mode')=='issue'){
				$query->join('INNER',$db->quoteName('#__alkasubscriptions_deadline') .
				' ON ('.$db->quoteName("#__alkasubscriptions_deadline.subscription").' = '.$db->quoteName("#__alkasubscriptions_subscription.subscriptionType").' AND '.
				$db->quoteName("#__alkasubscriptions_deadline.issue").' = '.$db->quoteName("#__alkasubscriptions_subscription.endIssueNumber").') ');
			}
			if($subscriptionId){
					$query->where($db->quoteName('subscriptionId') . ' = '. $db->quote($subscriptionId));
			}
			if($orderId){
                $query->join('left',$db->quoteName('#__alkasubscriptions_subscription_order').' ON '.$db->quoteName('#__alkasubscriptions_subscription_order.id_subscription').'='.$db->quoteName('#__alkasubscriptions_subscription.id'));
				$query->where($db->quoteName('#__alkasubscriptions_subscription_order.id_order') . ' = '. $db->quote($orderId));
			}
			$db->setQuery($query);
			$subscriptions = $db->loadObjectList();

			$now = new DateTime();
			if(sizeof($subscriptions)>0){
					$groupsToLeave = array();
					$groupsToJoin = array();

					foreach($subscriptions as $subscription){
							$usersToClean[] = $subscription->idUser;
							$endDate = new DateTime($subscription->endDate);

							//actual groups
							$groupsToJoin   = AlkaSubscriptionsHelper::_prepareGroups($subscription->userActualGroups, $subscription->idUser,$groupsToJoin,3);

							if($subscription->enabled == 0 || $subscription->paid == 0 || ( $subscription->forever == 0 && $endDate < $now) ){
									//subscription is ended
									$groupsToJoin   = AlkaSubscriptionsHelper::_prepareGroups($subscription->product_alkasubscription_groups_in_on_leave, $subscription->idUser,$groupsToJoin,2);
									$groupsToLeave  = AlkaSubscriptionsHelper::_prepareGroups($subscription->product_alkasubscription_groups_out_on_leave, $subscription->idUser,$groupsToLeave,2);
							}else{
									//subscription is enabled
									$groupsToJoin   = AlkaSubscriptionsHelper::_prepareGroups($subscription->product_alkasubscription_groups_in_on_enter, $subscription->idUser,$groupsToJoin,1);
									$groupsToLeave  = AlkaSubscriptionsHelper::_prepareGroups($subscription->product_alkasubscription_groups_out_on_enter, $subscription->idUser,$groupsToLeave,1);
							}
					}
					$queryValues = array();
					foreach($groupsToJoin as $userId=>$groups){
							foreach($groups as $groupId=>$priority){
									if(!isset($groupsToLeave[$userId][$groupId])||$groupsToLeave[$userId][$groupId]>$groupsToJoin[$userId][$groupId]){
											if(!empty($groupId)){
													$queryValues[]="(".$userId.",".$groupId.")";
											}
									}
							}
					}

					unset($groupsToLeave);
					unset($groupsToJoin);
					unset($subscriptions);

					$db->setQuery('DELETE FROM '.$db->quoteName('#__user_usergroup_map').' WHERE  '.$db->quoteName('user_id').' IN ('.implode(',',$usersToClean).'); ');
					if($db->execute()){
							$db->setQuery('INSERT INTO '.$db->quoteName('#__user_usergroup_map').' ( '.$db->quoteName('user_id').','.$db->quoteName('group_id').') VALUES '.implode(',',$queryValues).'; ');
							if($db->execute()){
									$return = true;
							}else{
									$return = false;
							}
					}else{
							$return = false;
					}
			}else{
					$return = true;
			}
			return $return;
	}

	public static function _prepareGroups($groupsToAdd, $userId,$result,$priority){
			$groupsToAdd = explode(",",$groupsToAdd);
			$groupsToAddPrep = array();
			foreach($groupsToAdd as $groupsToAdd){
					$groupsToAddPrep[$groupsToAdd]=$priority;
			}
			if (isset($result[$userId])){
					foreach($groupsToAddPrep as $groupToAdd=>$groupToAddPriority){
							if(!isset($result[$userId][$groupToAdd]) || $result[$userId][$groupToAdd]>$groupToAddPriority){
									$result[$userId][$groupToAdd]=$groupToAddPriority;
							}
					}
			}else{
					$result[$userId] = $groupsToAddPrep;
			}
			return $result;
	}

	public static function mail_notif($receivers,$subject,$body){
		$mailer = JFactory::getMailer();
		$config = JFactory::getConfig();
		$sender = array(
				$config->get( 'mailfrom' ),
				$config->get( 'fromname' )
		);

		$mailer->setSender($sender);
		$mailer->addRecipient($receivers);
        $mailer->addRecipient('copieboutique@foretnature.be');
		$mailer->setSubject($subject);
		$mailer->setBody($body);


		$mailer->isHTML(true);
		$mailer->Send();
	}

    public static function encryptData($encrypt){
        $params 	= JComponentHelper::getParams('com_alkasubscriptions');
        $key = md5($params->get('salt')).md5(md5($params->get('salt')));
        $encrypt = serialize($encrypt);
        $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC), MCRYPT_DEV_URANDOM);
        $key = pack('H*', $key);
        $mac = hash_hmac('sha256', $encrypt, substr(bin2hex($key), -32));
        $passcrypt = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $encrypt.$mac, MCRYPT_MODE_CBC, $iv);
        $encoded = base64_encode($passcrypt).'|'.base64_encode($iv);
        return $encoded;
    }

    public static function decryptData($decrypt){
        $params 	= JComponentHelper::getParams('com_alkasubscriptions');
        $key = md5($params->get('salt')).md5(md5($params->get('salt')));
        $decrypt = explode('|', $decrypt.'|');
        $decoded = base64_decode($decrypt[0]);
        $iv = base64_decode($decrypt[1]);
        if(strlen($iv)!==mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC)){ return false; }
        $key = pack('H*', $key);
        $decrypted = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $decoded, MCRYPT_MODE_CBC, $iv));
        $mac = substr($decrypted, -64);
        $decrypted = substr($decrypted, 0, -64);
        $calcmac = hash_hmac('sha256', $decrypted, substr(bin2hex($key), -32));
        if($calcmac!==$mac){ return false; }
        $decrypted = unserialize($decrypted);
        return $decrypted;
    }

    public static function getLastSent($type ){
        $db     = JFactory::getDBO();
        $query  = $db->getQuery(true);
        $query->select($db->quoteName('dl').'.*');
        $query->from($db->quoteName('#__alkasubscriptions_deadline', 'dl'));
        $query->where($db->quoteName('dl.date').' < NOW()');
        $query->where($db->quoteName('dl.subscription').' = '.$db->quote($type));
        $query->order($db->quoteName('dl.date').' DESC LIMIT 0,1');

        $db->setQuery($query);
        return $db->loadObject();
    }



    public static function getActiveSubscriptionQuery(){
        $config 	= JFactory::getConfig();
		$params 	= JComponentHelper::getParams('com_alkasubscriptions');
		$nowDate 	= new DateTime();

		$db     = JFactory::getDBO();
        $query  = $db->getQuery(true);


        $getLastSent = '(SELECT '.$db->quoteName('dl.issue').' FROM '.$db->quoteName('#__alkasubscriptions_deadline').' as dl WHERE '.$db->quoteName('date').' < NOW() AND '.$db->quoteName('dl.subscription').' = '.$db->quoteName('s.subscriptionType').' ORDER BY '.$db->quoteName('dl.date').' DESC LIMIT 0,1)';
        $query->select('s.*');
        $query->from($db->quoteName('#__alkasubscriptions_subscription','s'));
        if($params->get('mode')=='duration'){
            $query->where($db->quoteName('s.endDate') . ' = DATE_ADD(CURDATE(),INTERVAL '.$daysNbr.' DAY) ');
        }else if($params->get('mode')=='issue'){
            $query->select(array($getLastSent.' AS lastSent'));
            $query->where($getLastSent.'<='.$db->quoteName('s.endIssueNumber'));
        }
        $query->where($db->quoteName('s.enabled') . '=1');
        return $query;
    }

	public static function sendAlert($body, $daysNbr, $subject, $content, $buttonState, $buttonText, $footer){
		$config 	= JFactory::getConfig();
		$params 	= JComponentHelper::getParams('com_alkasubscriptions');
		$nowDate 	= new DateTime();

		$db     = JFactory::getDBO();
    $query  = $db->getQuery(true);

    $query->select(
        $db->quoteName(
            array(
                's.id',
    			's.name',
    			's.startDate',
    			's.endDate',
                's.subscriptionType',
    			'a.title',
                'a.company',
    			'a.firstname',
    			'a.lastname',
    			'a.addressLine1',
    			'a.addressLine2',
    			'a.zip',
    			'a.city',
    			'a.country',
                'u.email',
                'u.id',
                'p.product_alkasubscription_product_to_renew',
    			'p.product_name',
                //'s.idOrder'
            ),
            array(
                null,
    			null,
    			null,
    			null,
    			null,
                null,
                null,
    			null,
    			null,
    			null,
    			null,
    			null,
    			null,
    			null,
                null,
                'user_id',
                null,
    			null,
                //null
            )
        )
    );

    $query->from($db->quoteName('#__alkasubscriptions_subscription', 's'));
	$query->join('LEFT', $db->quoteName('#__alkasubscriptions_address', 'a')
		. ' ON (' .
		$db->quoteName('s.idAddress')
		. ' = ' .
		$db->quoteName('a.id') . ')');
    $query->join('INNER', $db->quoteName('#__users', 'u')
    . ' ON (' .
    $db->quoteName('s.idUser')
    . ' = ' .
    $db->quoteName('u.id') . ')');
	$query->join('LEFT', $db->quoteName('#__hikashop_product', 'p')
    . ' ON (' .
    $db->quoteName('s.idProduct')
    . ' = ' .
    $db->quoteName('p.product_id') . ')');
    if($params->get('mode')=='duration'){
        $query->where($db->quoteName('s.endDate') . ' = DATE_ADD(CURDATE(),INTERVAL '.$daysNbr.' DAY) ');
    }else if($params->get('mode')=='issue'){
        $query->join('LEFT', $db->quoteName('#__alkasubscriptions_deadline', 'dl')
        . ' ON (' .
        $db->quoteName('s.subscriptionType')
        . ' = ' .
        $db->quoteName('dl.subscription') . ')');
        $query->where(' DATE_ADD('.$db->quoteName('dl.date').', INTERVAL '.$daysNbr.' DAY) =CURDATE()');
        $query->where($db->quoteName('s.endIssueNumber').' = '.$db->quoteName('dl.issue'));
    }
    $query->where($db->quoteName('s.enabled') . '=1');
    $query->where($db->quoteName('s.sendNotification') . '=1');
    $query->where($db->quoteName('s.paid') . '=1');
    //$query->where($db->quoteName('s.id') . '=801');
    //developpement
    $db->setQuery((string) $query);
    $items = $db->loadObjectList();

    // Assign data to tags common to all alerts
    $tags['[COM_ALKASUBSCRIPTIONS_SITELOGO_LEFT]']   					= JUri::root().'/'.$params->get('logo_mail_left');
    $tags['[COM_ALKASUBSCRIPTIONS_SITELOGO_RIGHT]']  					= JUri::root().'/'.$params->get('logo_mail_right');
    $tags['[COM_ALKASUBSCRIPTIONS_SITEURL]']         					= JUri::root();
    $tags['[COM_ALKASUBSCRIPTIONS_SITENAME]']        					= $config->get( 'sitename' );
    $tags['[COM_ALKASUBSCRIPTIONS_MAIN_CONTENT]']    					= nl2br($content);
    $tags['[COM_ALKASUBSCRIPTIONS_BUTTON_TEXT]']         			    = $button;
    $tags['[COM_ALKASUBSCRIPTIONS_CUSTOM_MAIL_FOOTER_TEXT]'] 	        = $footer;

    $tags['[COM_ALKASUBSCRIPTIONS_MAIN_TITLE]']      					= $subject;


		foreach($tags as $k=>$val){
			$body = str_replace($k, $val, $body);
		}

		if(sizeof($items)){

      foreach($items as $item)
			{
                $billing_address='';
                if($item->idOrder){
                    $db     = JFactory::getDBO();
                    $query  = $db->getQuery(true);
                    $query->select(
                        array(
                            'ba.address_firstname',
                            'ba.address_lastname',
                            'ba.address_company',
                            'ba.address_street',
                            'ba.address_street2',
                            'ba.address_post_code',
                            'ba.address_city',
                            'c.zone_name'
                        )
                    );
                    $query->from($db->quoteName('#__hikashop_order', 'o'));
                    $query->join('LEFT', $db->quoteName('#__hikashop_address', 'ba')
                		. ' ON (' .
                		$db->quoteName('ba.address_id')
                		. ' = ' .
                		$db->quoteName('o.order_billing_address_id') . ')');
                    $query->join('LEFT', $db->quoteName('#__hikashop_zone', 'c')
                		. ' ON (' .
                		$db->quoteName('c.zone_namekey')
                		. ' = ' .
                		$db->quoteName('ba.address_country') . ')');
                    $query->where($db->quoteName('o.order_id') . '='  .$db->quote($item->idOrder));

                    $db->setQuery((string) $query);
                    $billing_address_array = $db->loadRow();
                    $billing_address = $billing_address_array->address_firstname.' '.$billing_address_array->address_lastname;
                    $billing_address .=($billing_address_array->address_company)?'<br/>'.$billing_address_array->address_company:'';
                    $billing_address .=($billing_address_array->address_street)?'<br/>'.$billing_address_array->address_street:'';
                    $billing_address .=($billing_address_array->address_street2)?'<br/>'.$billing_address_array->address_street2:'';
                    $billing_address .='<br/>'.$billing_address_array->address_post_code.' '.$billing_address_array->address_city. '<br/>'.$billing_address_array->zone_name;
                }



				$mybody = $body;

                $shipping_address = $item->firstname.' '.$item->lastname;
                $shipping_address .=($item->company)?'<br/>'.$item->company:'';
                $shipping_address .=($item->addressLine1)?'<br/>'.$item->addressLine1:'';
                $shipping_address .=($item->addressLine2)?'<br/>'.$item->addressLine2:'';
                $shipping_address .='<br/>'.$item->zip.' '.$item->city. '<br/>'.$item->country;

                switch($item->subscriptionType){
                    case 'SAL':
                        $nomRevue = 'La Salamandre';
                        $logoRevue = 'logomailSAL.jpg';
                        $coverRevue = 'covermailSAL.jpg';
                        $mailContactRevue = 'info@salamandrebelux.net';
                        $renewUrl = JUri::root().'index.php?option=com_hikashop&view=product&layout=listing&Itemid=1056';
                    break;

                    case 'SAJ':
                        $nomRevue = 'La Salamandre Junior';
                        $logoRevue = 'logomailSAJ.jpg';
                        $coverRevue = 'covermailSAJ.jpg';
                        $mailContactRevue = 'info@salamandrebelux.net';
                        $renewUrl = JUri::root().'index.php?option=com_hikashop&view=product&layout=listing&Itemid=1057';
                    break;

                    case 'PSA':
                        $nomRevue = 'La Petite Salamandre';
                        $logoRevue = 'logomailPSA.jpg';
                        $coverRevue = 'covermailPSA.jpg';
                        $mailContactRevue = 'info@salamandrebelux.net';
                        $renewUrl = JUri::root().'index.php?option=com_hikashop&view=product&layout=listing&Itemid=1058';
                    break;

                    case 'FO':
                        $nomRevue = 'Forêt.Nature';
                        $logoRevue = 'logomailFO.jpg';
                        $coverRevue = 'covermailFO.jpg';
                        $mailContactRevue = 'info@foretnature.be';
                        $renewUrl = JUri::root().'index.php?option=com_hikashop&view=category&layout=listing&Itemid=813';
                    break;
                }

				$a = array('[name]', '[endDate]', '[startDate]', '[firstname]', '[lastname]', '[addressLine1]', '[addressLine2]', '[zip]', '[city]', '[country]','[PRODUCT_NAME]','[SHIPPING_ADDRESS]','[BILLING_ADDRESS]','[NOM_REVUE]','[PRENOM_LIVRAISON]','[NOM_LIVRAISON]','[LOGO_REVUE]', '[NOM_ABONNEMENT]','[COVER_REVUE]','[MAIL_REVUE]');
				$b = array($item->name, $item->endDate, $item->startDate, $item->firstname, $item->lastname, $item->addressLine1, $item->addressLine2, $item->zip, $item->city, $item->country, $item->product_name,$shipping_address,$billing_address,$nomRevue , $item->firstname, $item->lastname,JUri::root().'/images/mails/'.$logoRevue, $item->name,JUri::root().'/images/mails/'.$coverRevue, $mailContactRevue );
				$htmlStruc = "<table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tbody>
                          <tr>
                            <td style='text-align: left;' colspan='2'>
														<br/>
                             <h2 style='font-size: 16px; font-weight:bold;'>[name]<br/><br/>
														 <span style='font-size:15px; font-weight:normal;'>". JText::_('COM_ALKASUBSCRIPTIONS_CUSTOM_MAIL_DETAILS_SENT_TO') ." [firstname] [lastname], [addressLine1] [addressLine2], [zip] [city] - [country]</span></h2>
                            </td>
                          </tr>
                          <tr>
                            <td style='text-align: left;'>
                              <p>". JText::_('COM_ALKASUBSCRIPTIONS_CUSTOM_MAIL_DETAILS_STARTED_ON') ." <strong>[startDate]</strong>,
                              ". JText::_('COM_ALKASUBSCRIPTIONS_CUSTOM_MAIL_DETAILS_EXPIRE_ON') ." <strong>[endDate]</strong></p>
                            </td>
                          </tr>
                        </tbody>
                      </table>";
				$button = "<tr>
							      <td>
							        <div style='text-align: center; margin-top: 40px;'>
							          <a href='[COM_ALKASUBSCRIPTIONS_CUSTOM_MAIL_BUTTON_LINK]' class='mailbutton'>[COM_ALKASUBSCRIPTIONS_CUSTOM_MAIL_BUTTON_TEXT]</a>
							        </div>
							      </td>
							    </tr>";
                $tsubject = str_replace($a, $b, $subject);
				$mybody = str_replace('[COM_ALKASUBSCRIPTIONS_CUSTOM_MAIL_DETAILS]', $details, $mybody);
                $mybody = str_replace($a, $b, $mybody);

				if($buttonState == 1){
                    $dataInLink = array(
                        'user_id' => $item->user_id,
                        'sub_id' => $item->id,
                        'salt' => $params->get('salt')
                    );

					$mybody = str_replace('[COM_ALKASUBSCRIPTIONS_CUSTOM_MAIL_BUTTON]', $button, $mybody);
					$mybody = str_replace('[COM_ALKASUBSCRIPTIONS_CUSTOM_MAIL_BUTTON_TEXT]', $buttonText, $mybody);
					//$mybody = str_replace('[COM_ALKASUBSCRIPTIONS_CUSTOM_MAIL_BUTTON_LINK]', $renewUrl.'&'.$params->get('publickey').'='.urlencode(AlkasubscriptionsHelper::encryptData($dataInLink)), $mybody);
                    //$mybody = str_replace('[COM_ALKASUBSCRIPTIONS_CUSTOM_MAIL_BUTTON_LINK]', JUri::root().'/index.php?'.$params->get('publickey').'='.urlencode(AlkasubscriptionsHelper::encryptData($dataInLink)).'&option=com_hikashop&ctrl=product&task=updatecart&quantity=1&Itemid=926&checkout=1&product_id='.$item->product_alkasubscription_product_to_renew, $mybody);
                    $mybody = str_replace('[COM_ALKASUBSCRIPTIONS_CUSTOM_MAIL_BUTTON_LINK]', JUri::root().'/index.php?'.$params->get('publickey').'='.urlencode(AlkasubscriptionsHelper::encryptData($dataInLink)).'&option=com_alkasubscriptions&view=subscriptions&Itemid=948', $mybody);
				} else {
					$mybody = str_replace('[COM_ALKASUBSCRIPTIONS_CUSTOM_MAIL_BUTTON]', '', $mybody);
				}
                AlkasubscriptionsHelper::mail_notif($item->email,$tsubject,$mybody);
				AlkasubscriptionsHelper::setMailHistory($tsubject, $mybody, $item->email, $item->id);

      }
			echo sizeof($items).' rappels pour '.$daysNbr.' jours envoyé.<br/>';
    } else {
      echo '0 rappels pour '.$daysNbr.' jours a envoyer.<br/>';
    }
	}

	public static function setMailHistory($subject, $body, $recipient, $idSubscription){
		// Get a db connection
		$db = JFactory::getDbo();

		// Create a new query object
		$query = $db->getQuery(true);

		$nowDate = new Datetime();

		// Insert columns
    $columns = array(
			'subject',
			'body',
			'recipient',
			'idSubscription',
			'sendDate');

    // Insert values
    $values = array(
			$db->quote($subject),
			$db->quote($body),
			$db->quote($recipient),
			$idSubscription,
			$db->quote($nowDate->format('Y-m-d H:i:s')));

    // Prepare the insert query
    $query
        ->insert($db->quoteName('#__alkasubscriptions_mail_history'))
        ->columns($db->quoteName($columns))
        ->values(implode(',', $values));

    // Set the query using our newly populated query object and execute it
    $db->setQuery($query);
    $result = $db->execute();

		if($result){
			return $db->insertid();
		} else {
			return false;
		}
	}
}
