<?php

/**
 * @version    CVS: 1.0.0
 * @package    com_alkasubscriptions
 * @author     Alka <developpement@alkaonline.be>
 * @copyright  2017 Alka
 * @license    GNU General Public License version 2 ou version ultérieure ; Voir LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;


/**
 * Class AlkaSubscriptionsController
 *
 * @since  1.6
 */
class AlkaSubscriptionsController extends JControllerLegacy
{

    public function importNew(){
        die();
        if(!@include_once(rtrim(JPATH_ADMINISTRATOR,DS).DS.'components'.DS.'com_hikashop'.DS.'helpers'.DS.'helper.php')){ die('test'); return false; }
        jimport( 'joomla.user.user' );
        $csv = array_map('str_getcsv', file(JPATH_SITE.'/superfile.csv'));
        unset($csv[0]);
        /*
            [0] => owner
            [1] => address_firstname
            [2] => address_lastname
            [3] => address_company
            [4] => address_street
            [5] => address_post_code
            [6] => address_city
            [7] => address_country
            [8] => product_id
            [9] => order_product_name
            [10] => order_product_code
            [11] => order_product_price
            [12] => order_product_tax
            */
        echo '<textarea>';
        print_r($csv);
        echo '<textarea>';
        //die();
        foreach ($csv as $row){
            $email_canonical = strtolower(trim($row[0]));
            $db = JFactory::getDbo();

            $query = $db->getQuery(true);
            $query->select($db->quoteName(array('user_id')));
            $query->from($db->quoteName('#__hikashop_user'));
            $query->where($db->quoteName('user_email') . ' LIKE '. $db->quote($email_canonical));
            $db->setQuery($query);
            $userid = $db->loadResult();

            $order = new stdClass();
            $order->order_user_id = $userid;
            $order->order_full_price = $row[11]+$row[12];
            $order->order_payment_id='2';
            $order->order_payment_method='banktransfercomm';
            $order->order_shipping_id='1';
            $order->order_shipping_method='manual';

            $address = new stdClass();
            $address->address_user_id = $userid;
            $address->address_firstname = $row[1];
            $address->address_lastname = $row[2];
            $address->address_company = $row[3];
            $address->address_street = $row[4];
            $address->address_post_code = $row[5];
            $address->address_city = $row[6];
            $address->address_published = 1;
            $address->address_country = 'country_Belgium_21';
            $addressClass = hikashop_get('class.address');

            $addressID = $addressClass->save($address);
            echo $addressID;
            echo '<pre>';
            print_r(get_class_methods($addressID));
            echo '</pre>';
            //die();
            $order->order_billing_address_id = $addressID;
            $order->order_shipping_address_id = $addressID;
            $orderClass = hikashop_get('class.order');
            $orderId = $orderClass->save($order);

            $product = new stdClass();
            $product->order_id = $orderId;
            $product->product_id = $row[8];
            $product->order_product_quantity = 1;
            $product->order_product_name = $row[9];
            $product->order_product_code = $row[10];
            $product->order_product_price = $row[11];
            $product->order_product_tax = $row[12];
            //$product->order_product_tax_info = 'a:1:{i:0;O:8:"stdClass":2:{s:11:"tax_namekey";s:4:"TVA6";s:10:"tax_amount";s:9:"2.0377356";}}';
            $orderProductClass = hikashop_get('class.order_product');
            $products = array($product);
            echo '<pre>';
            print_r($products);
            echo '</pre>';
            $orderProductClass->save($products);

			$update = new stdClass();
			$update->order_id = $orderId;
			$update->order_status = 'canceled';
			$update->history = new stdClass();
			$update->history->history_notified = 0;
			$orderClass->save($update);

			$update->order_status = 'created';
			$orderClass->save($update);

            $update->order_status = 'confirmed';
			$orderClass->save($update);
        }


        die();
        jimport( 'joomla.user.user' );
        $csv = array_map('str_getcsv', file(JPATH_SITE.'/ab_new.csv'));
        unset($csv[0]);
        $c = 0;
        echo "<table border='1'>";
        echo "<tr>";
        echo "<td>userId</td>";
        echo "<td>addMail</td>";
        echo "<td>state</td>";
        echo "</tr>";

        foreach($csv as $row){
            echo "<tr>";
            $email_canonical = strtolower(trim($row[2]));
            $db = JFactory::getDbo();

            $query = $db->getQuery(true);
            $query->select($db->quoteName(array('id', 'email')));
            $query->from($db->quoteName('#__users'));
            $query->where($db->quoteName('email') . ' LIKE '. $db->quote($email_canonical));
            $db->setQuery($query);
            $result = $db->loadObject();

            if(!$result){
                if (filter_var($email_canonical, FILTER_VALIDATE_EMAIL)) {
                    $c++;
                    //Création de l'instance d'un USER
    				$instance = JUser::getInstance();
    				$instance->set('id'      , 0);
    				$instance->set('name'    , trim($row[0]).' '.trim($row[1]));
    				$instance->set('username', $email_canonical);
    				$instance->set('password_clear', $row[3]);
    				$instance->set('email'   , $email_canonical);  // Result should contain an email (check)
    				$instance->set('groups'  , array(2,14));
    				$instance->set('block'  , 0);

    				//Sauvegarde de l'instance dans la DB
        			$result = 	$instance->save();

        			echo '<td>'.'</td><td>'.$email_canonical.'</td><td>inserted</td>';
                }else{
                    echo '<td>'.'</td><td>'.$email_canonical.'</td><td>NOT VALID</td>';
                }
            }else{
                $c++;
                echo '<td>'.$result->id.'</td><td>'.$email_canonical.'</td><td>existed</td>';
            }
            echo "</tr>";
        }

        echo "</table>";
    }

    public function importSAL(){
      die();
        jimport( 'joomla.user.user' );
        $csv = array_map('str_getcsv', file(JPATH_SITE.'/sal.csv'));
        /*
            [0] => ABO-REF
            [1] => ABO-TYPE
            [2] => date DÉBUT
            [3] => date FIN
            [4] => E-MAIL
            [5] => Livraison-SOCIÉTÉ
            [6] => Livraison-NOM
            [7] => Livraison-RUE
            [8] => Livraison-CP
            [9] => Livraison-COMMUNE
            [10] => Livraison-PAYS
            [11] => tiers payeur
            [12] => Facturation-SOCIÉTÉ
            [13] => Facturation-NOM
            [14] => Facturation-RUE
            [15] => Facturation-CP
            [16] => Facturation-COMMUNE
            [17] => Facturation-PAYS
            [18] => TEL
            [19] => TVA
            [20] => Password
         */
        unset($csv[0]);

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query->select(array('*'));
        $query->from($db->quoteName('#__users'));

        $db->setQuery($query);
        $users = $db->loadObjectList('email');
        /*echo "<textarea>";
        print_r($users);
        echo "</textarea>";
        foreach($csv as $row){
                $email_canonical = strtolower(trim($row[4]));
                if(!isset($users[$email_canonical])){


                if (filter_var($email_canonical, FILTER_VALIDATE_EMAIL)) {

                    //Création de l'instance d'un USER
    				$instance = JUser::getInstance();
    				$instance->set('id'      , 0);
    				$instance->set('name'    , trim($row[6]));
    				$instance->set('username', $email_canonical);
    				$instance->set('password_clear', $row[20]);
    				$instance->set('email'   , $email_canonical);  // Result should contain an email (check)
    				$instance->set('groups'  , array(15));
    				$instance->set('block'  , 0);

    				//Sauvegarde de l'instance dans la DB
        			$result = 	$instance->save();
                }else{
                    echo '<br/>'.$email_canonical.':not valid</br>';
                }
                }
        }*/
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query->select(array('#__hikashop_product.product_code','#__hikashop_product.product_id'));
        $query->from($db->quoteName('#__hikashop_product'));
        $db->setQuery($query);
        $products = $db->loadObjectList('product_code');

            $now = new DateTime();
            foreach($csv as &$row){
                $email_canonical = strtolower(trim($row[4]));
                // Create and populate an object.
                $address = new stdClass();
                $address->title=null;
                $address->firstname=$row[6];
                $address->lastname ='';
                $address->company =$row[5];
                $address->addressLine1 =$row[14];
                $address->addressLine2 =null;
                $address->city =$row[9];
                $address->zip =$row[8];
                $address->country =$row[10];
                $address->createdDate =$now->format('Y-m-d H:i:s');
                $address->createdBy = $users[$email_canonical]->id;
                $address->modifiedDate=null;
                $address->modifiedBy=null;
                $address->enabled=1;
                $address->note=null;
                $address->idUser = $users[$email_canonical]->id;

                // Insert the object into the user profile table.
                $result = JFactory::getDbo()->insertObject('#__alkasubscriptions_address', $address);

                $addressid = $db->insertid();
              echo "<textarea>";
              print_r($address);
              echo "</textarea>";
                $sub = new StdClass();
                $sub->name =$row[0];
                $sub->createdDate =$row[2];
                $sub->createdBy = $users[$email_canonical]->id;
                $sub->startDate= $row[2];
                $sub->endDate =$row[3];
                $sub->renewDate =null;
                $sub->forever =0;
                $sub->enabled =1;
                $sub->idUser = $users[$email_canonical]->id;
                $sub->idProduct =$products[$sub->name]->product_id;
                $sub->idOrder  =null;
                $sub->idAddress =$addressid;
                $sub->paid =true;
                $sub->productOptions=serialize(array("type"=>$row[1]));

                $result = JFactory::getDbo()->insertObject('#__alkasubscriptions_subscription', $sub);

              echo "<textarea>";
              print_r($sub);
              echo "</textarea>";
            }

        die('end');
    }

    public function importMassive(){
        die();
        $csv = array_map('str_getcsv', file(JPATH_SITE.'/subs.csv'));
        /*
        [0] => ID de l'abonnement
        [1] => Titre de l'abonnement
        [2] => ID client Hikashop
        [3] => Email
        [4] => Date de début
        [5] => Date de fin
        [6] => Nombre d'addresse actives
        [7] => Id de l'adresse à utiliser
        [8] => Prénom
        [9] => Nom
        [10] => Société
        [11] => Adresse 1
        [12] => Adresse 2
        [13] => Ville
        [14] => Code Postal
        [12] => Country
         */
        unset($csv[0]);
        /*echo "<textarea>";
        print_r($csv);
        echo "</textarea>";*/
        echo "<table border='1'>";
            echo '<thead>';
            echo '<tr>';
                echo '<th>[0] => ID de l\'abonnement</th>';
                echo '<th>[1] => Titre de l\'abonnement</th>';
                echo '<th>[2] => ID client Hikashop</th>';
                echo '<th>[3] => Email</th>';
                echo '<th>[4] => Date de début</th>';
                echo '<th>[5] => Date de fin</th>';
                //echo '<th>[6] => Nombre d\'addresse actives</th>';
                echo '<th>[7] => Id de l\'adresse à utiliser</th>';
                echo '<th>[8] => Prénom</th>';
                echo '<th>[9] => Nom</th>';
                echo '<th>[10] => Société</th>';
                echo '<th>[11] => Adresse 1</th>';
                echo '<th>[12] => Adresse 2</th>';
                echo '<th>[13] => Ville</th>';
                echo '<th>[14] => Code Postal</th>';
                echo '<th>[15] => Country</th>';
            echo '</tr>';
            echo '</thead>';
            echo '<tbody>';
            $now = new DateTime();
            foreach($csv as &$row){
                $address=null;
                $row[15]='';
                if($row[6]==1){


                    $db = JFactory::getDbo();
                    $query = $db->getQuery(true);

                    $query->select(array('#__hikashop_address.*','#__hikashop_zone.zone_name'));
                    $query->join('LEFT','#__hikashop_zone ON #__hikashop_zone.zone_namekey = #__hikashop_address.address_country');
                    $query->from($db->quoteName('#__hikashop_address'));
                    $query->where($db->quoteName('#__hikashop_address.address_user_id').' = '.$row[2]);
                    $query->where($db->quoteName('#__hikashop_address.address_published').' = 1');

                    $db->setQuery($query);
                    $address = $db->loadObject();
                    $row[8] = $address->address_firstname;
                    $row[9] = $address->address_lastname;
                    $row[10] = $address->address_company;
                    $row[11] = $address->address_street;
                    $row[12] = $address->address_street2;
                    $row[13] = $address->address_city;
                    $row[14] = $address->address_post_code;
                    $row[15] = $address->zone_name;
                }else if(!empty($row[7])){
                    $db = JFactory::getDbo();
                    $query = $db->getQuery(true);

                    $query->select(array('#__hikashop_address.*','#__hikashop_zone.zone_name'));
                    $query->join('LEFT','#__hikashop_zone ON #__hikashop_zone.zone_namekey = #__hikashop_address.address_country');
                    $query->from($db->quoteName('#__hikashop_address'));
                    $query->where($db->quoteName('#__hikashop_address.address_id').' = '.$row[7]);

                    $db->setQuery($query);
                    $address = $db->loadObject();
                    $row[8] = $address->address_firstname;
                    $row[9] = $address->address_lastname;
                    $row[10] = $address->address_company;
                    $row[11] = $address->address_street;
                    $row[12] = $address->address_street2;
                    $row[13] = $address->address_city;
                    $row[14] = $address->address_post_code;
                    $row[15] = $address->zone_name;
                }
                $addressid = null;
                if(!empty($row[14])){
                    $db = JFactory::getDbo();
                    $query = $db->getQuery(true);

                    $query->select('#__hikashop_user.user_cms_id');
                    $query->from($db->quoteName('#__hikashop_user'));
                    $query->where($db->quoteName('#__hikashop_user.user_id').' = '.$row[2]);

                    $db->setQuery($query);
                    $userid = $db->loadResult();
                    // Create and populate an object.
                    $address = new stdClass();
                    $address->title=null;
                    $address->firstname=$row[8];
                    $address->lastname =$row[9];
                    $address->company =$row[10];
                    $address->addressLine1 =$row[11];
                    $address->addressLine2 =$row[12];
                    $address->city =$row[13];
                    $address->zip =$row[14];
                    $address->country =$row[15];
                    $address->createdDate =$now->format('Y-m-d H:i:s');
                    $address->createdBy = $userid;
                    $address->modifiedDate=null;
                    $address->modifiedBy=null;
                    $address->enabled=1;
                    $address->note=null;
                    $address->idUser = $userid;

                    // Insert the object into the user profile table.
                    //$result = JFactory::getDbo()->insertObject('#__alkasubscriptions_address', $address);

                    $addressid = $db->insertid();
                }

                $sub = new StdClass();
                $sub->name =$row[1];
                $sub->createdDate =$row[4];
                $sub->createdBy = $userid;
                $sub->startDate= $row[4];
                $sub->endDate =$row[5];
                $sub->renewDate =null;
                $sub->forever =0;
                $sub->enabled =1;
                $sub->idUser = $userid;
                $sub->idProduct =1;
                $sub->idOrder  =null;
                $sub->idAddress =$addressid;
                $sub->paid =true;
                $sub->productOptions=null;

                //$result = JFactory::getDbo()->insertObject('#__alkasubscriptions_subscription', $sub);
                echo '<tr style="'.( empty($row[14])?'background:orange':'' ).'">';
                    echo '<td>'.$row[0].'</td>';
                    echo '<td>'.$row[1].'</td>';
                    echo '<td>'.$row[2].'</td>';
                    echo '<td>'.$row[3].'</td>';
                    echo '<td>'.$row[4].'</td>';
                    echo '<td>'.$row[5].'</td>';
                    //echo '<td>'.$row[6].'</td>';
                    echo '<td>'.$row[7].'</td>';
                    echo '<td>'.$row[8].'</td>';
                    echo '<td>'.$row[9].'</td>';
                    echo '<td>'.$row[10].'</td>';
                    echo '<td>'.$row[11].'</td>';
                    echo '<td>'.$row[12].'</td>';
                    echo '<td>'.$row[13].'</td>';
                    echo '<td>'.$row[14].'</td>';
                    echo '<td>'.$row[15].'</td>';
                echo '</tr>';
            }
            echo '</tbody>';
        echo "</table>";
        die('end');
    }
    public function testVar(){
      $params = JComponentHelper::getParams('com_alkasubscriptions');
      var_dump($params->get('first_alert_mailcustom_days_before', 30));
      var_dump($params->get('second_alert_mailcustom_days_before', 15));
      var_dump($params->get('final_alert_mailcustom_days_before', 0));
      $daysNbr  = $params->get('second_alert_mailcustom_days_before', 15);
      $config 	= JFactory::getConfig();
      $params 	= JComponentHelper::getParams('com_alkasubscriptions');
      $nowDate 	= new DateTime();

      $db     = JFactory::getDBO();
      $query  = $db->getQuery(true);

      $query->select(
          $db->quoteName(
              array(
                  's.id',
            's.name',
            's.startDate',
            's.endDate',
                  's.subscriptionType',
            'a.title',
                  'a.company',
            'a.firstname',
            'a.lastname',
            'a.addressLine1',
            'a.addressLine2',
            'a.zip',
            'a.city',
            'a.country',
                  'u.email',
                  'u.id',
                  'p.product_alkasubscription_product_to_renew',
            'p.product_name',
                  //'s.idOrder'
              ),
              array(
                  null,
            null,
            null,
            null,
            null,
                  null,
                  null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
                  null,
                  'user_id',
                  null,
            null,
                  //null
              )
          )
      );

      $query->from($db->quoteName('#__alkasubscriptions_subscription', 's'));
    $query->join('LEFT', $db->quoteName('#__alkasubscriptions_address', 'a')
      . ' ON (' .
      $db->quoteName('s.idAddress')
      . ' = ' .
      $db->quoteName('a.id') . ')');
      $query->join('INNER', $db->quoteName('#__users', 'u')
      . ' ON (' .
      $db->quoteName('s.idUser')
      . ' = ' .
      $db->quoteName('u.id') . ')');
    $query->join('LEFT', $db->quoteName('#__hikashop_product', 'p')
      . ' ON (' .
      $db->quoteName('s.idProduct')
      . ' = ' .
      $db->quoteName('p.product_id') . ')');
      if($params->get('mode')=='duration'){
          $query->where($db->quoteName('s.endDate') . ' = DATE_ADD(CURDATE(),INTERVAL '.$daysNbr.' DAY) ');
      }else if($params->get('mode')=='issue'){
          $query->join('LEFT', $db->quoteName('#__alkasubscriptions_deadline', 'dl')
          . ' ON (' .
          $db->quoteName('s.subscriptionType')
          . ' = ' .
          $db->quoteName('dl.subscription') . ')');
          //$query->where($db->quoteName('dl.date').' = DATE_ADD(CURDATE(), INTERVAL '.$daysNbr.' DAY)');
          $query->where(' DATE_ADD('.$db->quoteName('dl.date').', INTERVAL '.$daysNbr.' DAY) =CURDATE()');
          $query->where($db->quoteName('s.endIssueNumber').' = '.$db->quoteName('dl.issue'));
      }
      $query->where($db->quoteName('s.enabled') . '=1');
      $query->where($db->quoteName('s.sendNotification') . '=1');
      echo '<hr/>';
      echo $query;
      die();
    }
  public function emailAlert(){
    $params = JComponentHelper::getParams('com_alkasubscriptions');

    AlkasubscriptionsHelper::sendAlert(
      $params->get('first_alert_mailcustom_template',file_get_contents(JPATH_SITE.'/components/com_alkasubscriptions/assets/mails/basic/first_alert.html')),
      $params->get('first_alert_mailcustom_days_before', 30),
      $params->get('first_alert_mailcustom_subject', str_replace('[days]', $params->get('first_alert_mailcustom_days_before', 30), JText::_('COM_ALKASUBSCRIPTIONS_CUSTOM_MAIL_SUBJECT_DEFAULT'))),
      $params->get('first_alert_mailcustom_content', str_replace('[days]', $params->get('first_alert_mailcustom_days_before', 30), JText::_('COM_ALKASUBSCRIPTIONS_CUSTOM_MAIL_CONTENT_DEFAULT'))),
      $params->get('first_alert_mailcustom_button_state'),
      $params->get('first_alert_mailcustom_button_text', JText::_('COM_ALKASUBSCRIPTIONS_CUSTOM_MAIL_BUTTON_TEXT_DEFAULT')),
      $params->get('first_alert_mailcustom_footer')
  );
    AlkasubscriptionsHelper::sendAlert(
      $params->get('second_alert_mailcustom_template',file_get_contents(JPATH_SITE.'/components/com_alkasubscriptions/assets/mails/basic/first_alert.html')),
      $params->get('second_alert_mailcustom_days_before', 15),
      $params->get('second_alert_mailcustom_subject', str_replace('[days]', $params->get('second_alert_mailcustom_days_before',15), JText::_('COM_ALKASUBSCRIPTIONS_CUSTOM_MAIL_SUBJECT_DEFAULT'))),
      $params->get('second_alert_mailcustom_content', str_replace('[days]', $params->get('second_alert_mailcustom_days_before',15), JText::_('COM_ALKASUBSCRIPTIONS_CUSTOM_MAIL_CONTENT_DEFAULT'))),
      $params->get('second_alert_mailcustom_button_state'),
      $params->get('second_alert_mailcustom_button_text', JText::_('COM_ALKASUBSCRIPTIONS_CUSTOM_MAIL_BUTTON_TEXT_DEFAULT')),
      $params->get('second_alert_mailcustom_footer')
    );
    AlkasubscriptionsHelper::sendAlert(
      $params->get('final_alert_mailcustom_template',file_get_contents(JPATH_SITE.'/components/com_alkasubscriptions/assets/mails/basic/first_alert.html')),
      $params->get('final_alert_mailcustom_days_before', 0),
      $params->get('final_alert_mailcustom_subject', str_replace('[days]', $params->get('final_alert_mailcustom_days_before',0), JText::_('COM_ALKASUBSCRIPTIONS_CUSTOM_MAIL_SUBJECT_DEFAULT_FINAL'))),
      $params->get('final_alert_mailcustom_content', str_replace('[days]', $params->get('final_alert_mailcustom_days_before',0), JText::_('COM_ALKASUBSCRIPTIONS_CUSTOM_MAIL_CONTENT_DEFAULT_FINAL'))),
      $params->get('final_alert_mailcustom_button_state'),
      $params->get('final_alert_mailcustom_button_text', JText::_('COM_ALKASUBSCRIPTIONS_CUSTOM_MAIL_BUTTON_TEXT_DEFAULT')),
      $params->get('final_alert_mailcustom_footer')
  );
  }

  public function setGroups(){
      AlkasubscriptionsHelper::setGroups();
  }

    public function switchToNumber(){
	die();
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query->select(array('#__alkasubscriptions_subscription.*','#__hikashop_product.product_code'));
        $query->from($db->quoteName('#__alkasubscriptions_subscription'));
        $query->join('LEFT','#__hikashop_product ON ('.$db->quoteName('#__alkasubscriptions_subscription.idProduct').' = '.$db->quoteName('#__hikashop_product.product_id').')');

        $db->setQuery($query);
        $subscriptions = $db->loadObjectList();

        /****/
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query->select(array('#__alkasubscriptions_deadline.*'));
        $query->from($db->quoteName('#__alkasubscriptions_deadline'))->orderBy('date ASC');

        $db->setQuery($query);
        $deadlines = $db->loadObjectList();

        $deadlinesByType = array();

        foreach($deadlines as $deadline){
            $deadline->date = DateTime::createFromFormat('Y-m-d', $deadline->date);
            $deadlinesByType[$deadline->subscription][]=$deadline;
        }



        // Set subscriptionType to old subscription
        foreach($subscriptions as &$subscription){
            $subscription->startDate    = DateTime::createFromFormat('Y-m-d', $subscription->startDate);
            $subscription->endDate      = DateTime::createFromFormat('Y-m-d', $subscription->endDate);
            switch($subscription->product_code){
                case 'FA-FO-1AN':
                case 'FA-FO-2ANS':
                case 'FA-FO-1AN-STUDENT':
                    $subscriptionType='FO';
                break;

                case 'SA-PSA-1AN-NEW':
                case 'SA-PSA-1AN-REABO':
                case 'SA-PSA-2ANS-REABO':
                    $subscriptionType='PSA';
                break;

                case 'SA-SAJ-1AN-NEW':
                case 'SA-SAJ-1AN-REABO':
                case 'SA-SAJ-2ANS-REABO':
                    $subscriptionType='SAJ';
                break;

                case 'SA-SAL-1AN-NEW':
                case 'SA-SAL-1AN-REABO':
                case 'SA-SAL-2ANS-REABO':
                case 'SA-SAL+DVD-1AN-NEW':
                case 'SA-SAL+DVD-1AN-REABO':
                case 'SA-SAL+DVD-2ANS-REABO':
                    $subscriptionType='SAL';
                break;
            }
            $subscription->subscriptionType = $subscriptionType;
            $deadlines = $deadlinesByType[$subscription->subscriptionType];
            $thisDeadlines = array();
            foreach($deadlines as $deadline){
                if($subscription->startDate < $deadline->date && $subscription->endDate >= $deadline->date ){
                    $thisDeadlines[]=$deadline;
                }
            }
            ksort($thisDeadlines);
            $subscription->startIssueNumber = $thisDeadlines[0]->issue;
            $subscription->endIssueNumber = end($thisDeadlines)->issue;

            unset($subscription->product_code);
            JFactory::getDbo()->updateObject('#__alkasubscriptions_subscription', $subscription, 'id');
            if($subscription->idOrder){
                $link = new StdClass();
                $link->id_order = $subscription->idOrder;
                $link->id_subscription = $subscription->id;
                JFactory::getDbo()->insertObject('#__alkasubscriptions_subscription_order', $link);
            }
        }
    }

    public function switchToNumber2(){
	die();
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query->select(array('#__hikashop_product.*'));
        $query->from($db->quoteName('#__hikashop_product'));
        $query->where($db->quoteName('product_alkasubscription').'='.$db->quote(1));
        $db->setQuery($query);
        $products = $db->loadObjectList();


        // Set subscriptionType to old subscription
        foreach($products as &$product){

            switch($product->product_code){
                case 'FA-FO-1AN':
                    $product->product_alkasubscription_issue_quantity = 4;
                    $product->product_alkasubscription_type = 'FO';
                break;
                case 'FA-FO-1AN-NEW':
                    $product->product_alkasubscription_issue_quantity = 6;
                    $product->product_alkasubscription_type = 'FO';
                break;
                case 'FA-FO-2ANS':
                    $product->product_alkasubscription_issue_quantity = 8;
                    $product->product_alkasubscription_type = 'FO';
                break;
                case 'FA-FO-1AN-STUDENT':
                    $product->product_alkasubscription_issue_quantity = 4;
                    $product->product_alkasubscription_type = 'FO';
                break;

                case 'SA-PSA-1AN':
                    $product->product_alkasubscription_issue_quantity = 6;
                    $product->product_alkasubscription_type = 'PSA';
                break;
                case 'SA-PSA-1AN-NEW':
                    $product->product_alkasubscription_issue_quantity = 6;
                    $product->product_alkasubscription_type = 'PSA';
                break;
                case 'SA-PSA-1AN-REABO':
                    $product->product_alkasubscription_issue_quantity = 6;
                    $product->product_alkasubscription_type = 'PSA';
                break;
                case 'SA-PSA-2ANS-REABO':
                    $product->product_alkasubscription_issue_quantity = 12;
                    $product->product_alkasubscription_type = 'PSA';
                break;

                case 'SA-SAJ-1AN-NEW':
                    $product->product_alkasubscription_issue_quantity = 6;
                    $product->product_alkasubscription_type ='SAJ';
                break;
                case 'SA-SAJ-1AN':
                    $product->product_alkasubscription_issue_quantity = 6;
                    $product->product_alkasubscription_type ='SAJ';
                break;
                case 'SA-SAJ-1AN-REABO':
                    $product->product_alkasubscription_issue_quantity = 6;
                    $product->product_alkasubscription_type ='SAJ';
                break;
                case 'SA-SAJ-2ANS-REABO':
                    $product->product_alkasubscription_issue_quantity = 12;
                    $product->product_alkasubscription_type ='SAJ';
                break;

                case 'SAL-1AN-NEW':
                    $product->product_alkasubscription_issue_quantity = 6;
                    $product->product_alkasubscription_type ='SAL';
                break;
                case 'SAL-1AN':
                    $product->product_alkasubscription_issue_quantity = 6;
                    $product->product_alkasubscription_type ='SAL';
                break;
                case 'SA-SAL-1AN-REABO':
                    $product->product_alkasubscription_issue_quantity = 6;
                    $product->product_alkasubscription_type ='SAL';
                break;
                case 'SA-SAL-2ANS-REABO':
                    $product->product_alkasubscription_issue_quantity = 12;
                    $product->product_alkasubscription_type ='SAL';
                break;
                case 'SA-SAL+DVD-1AN':
                    $product->product_alkasubscription_issue_quantity = 6;
                    $product->product_alkasubscription_type ='SAL';
                break;
                case 'SA-SAL+DVD-1AN-NEW':
                    $product->product_alkasubscription_issue_quantity = 6;
                    $product->product_alkasubscription_type ='SAL';
                break;
                case 'SA-SAL+DVD-1AN-REABO':
                    $product->product_alkasubscription_issue_quantity = 6;
                    $product->product_alkasubscription_type ='SAL';
                break;
                case 'SA-SAL+DVD-2ANS-REABO':
                    $product->product_alkasubscription_issue_quantity = 12;
                    $product->product_alkasubscription_type ='SAL';
                break;
            }

            JFactory::getDbo()->updateObject('#__hikashop_product', $product, 'product_id');
        }
    }

}
