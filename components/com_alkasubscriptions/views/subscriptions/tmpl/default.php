<?php
/**
 * @version    CVS: 1.0.0
 * @package    com_alkasubscriptions
 * @author     Alka <developpement@alkaonline.be>
 * @copyright  2017 Alka
 * @license    GNU General Public License version 2 ou version ultérieure ; Voir LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
JHtml::_('jquery.framework');
$params     = JComponentHelper::getParams('com_alkasubscriptions');
$document = JFactory::getDocument();
$document->addScript('https://unpkg.com/sweetalert/dist/sweetalert.min.js');
$document->addScriptDeclaration('
    function extend(idSub,idProduct){
        console.log(idProduct);
        let productsToExtend = '.json_encode($this->productsToExtend).';
        console.log(productsToExtend);
        let field = document.createElement(\'select\');
        let attId = document.createAttribute("id");
        let attClass = document.createAttribute("class");
        attId.value="selectProductToExtend";
        attClass.value="input-block-level";
        field.setAttributeNode(attId);
        field.setAttributeNode(attClass);
        if(typeof productsToExtend[idProduct] != "undefined"){
          window.location="index.php?option=com_hikashop&ctrl=product&task=updatecart&add=1&product_id="+idProduct+"&subToRenew="+idSub+"&Itemid=1058&emptyCart=1";
            /*for(o = 0; o <= productsToExtend[idProduct].length-1; o++){
                let product = productsToExtend[idProduct][o];
                let option = document.createElement(\'option\');
                let optContent = document.createTextNode(product["product_name"]);
                option.appendChild(optContent);
                let att = document.createAttribute("value");
                att.value=product["product_id"];
                option.setAttributeNode(att);
                field.appendChild(option);
            }



            swal({
                title: "Comment prolonger cet abonnement ?",
                //text: "Choisissez dans la liste ci-dessous un produit permettant d\'étendre l\'abonnement. Ensuite cliquer sur \"Commander un renouvellement\". Lors de l\étape finale de la commande vous aurez la possibilité d\'assigner ce renouvelement à un de vos abonnements actifs.",
                icon: "info",
                content: field,
                buttons: true,
                dangerMode: true,
                buttons: ["Non merci", "Commander un renouvellement"]
            })
            .then((next) => {
                if (next) {
                    let f = document.getElementById("selectProductToExtend");
                    window.location="index.php?option=com_hikashop&ctrl=product&task=updatecart&add=1&product_id="+idProduct+"&subToRenew="+idSub+"&Itemid=1058";
                }
            });*/
        }else{
            swal({
                title: "Problème de paramètrage !",
                text: "Aucun produit disponnible pour renouveler votre abonnement",
                icon: "info",
                buttons: true,
                dangerMode: true,
                buttons: ["Ok"]
            });
        }

    }
');


?>
<div id="AlkaSubscriptions">
  <h2><?php echo JComponentHelper::getParams('com_alkasubscriptions')->get('component_name'); ?></h1>
  <ul class="nav nav-tabs">
    <li class="<?php if(JFactory::getApplication()->input->get('tab','subscriptions',null)=='subscriptions'){echo 'active';} ?>">
      <a data-toggle="tab" href="#subscriptions"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_SUBSCRIPTIONS'); ?></a>
    </li>
    <li class="<?php if(JFactory::getApplication()->input->get('tab',null,null)=='addresses'){echo 'active';} ?>">
      <a data-toggle="tab" href="#addresses"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_ADDRESSES'); ?></a>
    </li>
  </ul>

  <!-- TABS CONTENT -->
  <div class="tab-content">
    <div id="subscriptions" class="tab-pane fade <?php if(JFactory::getApplication()->input->get('tab','subscriptions',null)=='subscriptions'){echo 'in active';} ?>">
      <div class="row-fluid">
        <div class="span12">
          <table class="table table-hover">
            <thead>
              <th style="display:none;">#</th>
              <th><?php echo JText::_('COM_ALKASUBSCRIPTIONS_ADDRESS'); ?></th>

              <th><?php echo JText::_('COM_ALKASUBSCRIPTIONS_SUBSCRIPTION'); ?></th>
              <!--<th class="center"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_TYPE'); ?></th>-->
              <!-- <th style="white-space:nowrap;"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_START_DATE'); ?></th> -->
              <?php if($params->get('mode')=='duration'){ ?>
                  <th style="white-space:nowrap;"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_END_DATE'); ?></th>
              <?php } else if($params->get('mode')=='issue'){ ?>
                  <!-- <th style="white-space:nowrap;"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_START_ISSUE'); ?></th> -->
                  <!-- <th style="white-space:nowrap;"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_END_ISSUE'); ?></th> -->
                  <!-- <th style="white-space:nowrap;"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_LAST_SENT_ISSUE'); ?></th> -->
              <?php } ?>
              <th style="width:1%"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_PAID'); ?></th>
              <?php if($params->get('mode')=='issue' && $lastSent->issue <= $subscription->endIssueNumber ){ ?>
                  <th style="width:1%"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_EXTEND'); ?></th>
              <?php } ?>
            </thead>
            <tbody>
              <?php
                $row = 0;
                foreach ($this->subscriptions as $subscription) {
                      $lastSent = AlkaSubscriptionsHelper::getLastSent($subscription->subscriptionType);
                      $lastSentDate = new DateTime($lastSent->date);
                      $row++;
              ?>
              <tr>
                <td style="display:none;"><?php echo $row ?></td>
                <td>
					<address class="addresse<?php echo $subscription->idAdd ?>">
                        <strong><?php echo $subscription->firstname ?> <?php echo $subscription->lastname ?></strong><br/>
						<?php echo $subscription->addressLine1 ?></br>
						<?php if($subscription->addressLine2){ echo $subscription->addressLine2 . '</br>'; } ?>
						<?php echo $subscription->zip ?>
						<?php echo $subscription->city. '('.$subscription->paid.')' ?>
					</address>
                    <button type="button" onclick="linkAddressShow(<?php echo $subscription->idSub ?>,<?php echo $subscription->idAdd ?>)" class="btn btn-primary"><i class="fa fa-edit" aria-hidden="true"></i> <?php echo JText::_('COM_ALKASUBSCRIPTIONS_CHANGE'); ?> </button>
				</td>
                <td>
                    <strong><?php echo $subscription->name ?></strong><br/>
                    <?php if($subscription->paid == 1) { $startDate = new DateTime($subscription->startDate); echo '<small><span class="muted">'.JText::_('COM_ALKASUBSCRIPTIONS_CREATED_DATE').' :</span> '.$startDate->format('d/m/y').'</small><br/>'; }?>
                    <small><span class="muted"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_START_ISSUE'); ?> : </span> <?php echo $subscription->startIssueNumber; ?></small><br/>
                    <small><span class="muted"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_END_ISSUE'); ?> : </span> <?php echo $subscription->endIssueNumber; ?></small><br/>
                    <small><span class="muted"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_LAST_SENT_ISSUE'); ?> : </span> <?php echo $lastSent->issue.' ('.$lastSentDate->format('d/m/y').')'; ?></small>
                    <?php $futureRenews = AlkasubscriptionsHelper::getRenewOrdersBySubscription($subscription,false,true);
                    if(sizeof($futureRenews)>0){
                      echo '<p><strong>Prolongation(s) programmée(s)</strong><br/><small>';
                      echo '<table class="table  table-condensed">';
                      echo '<thead>';
                        echo '<tr>';
                          echo '<th>Produit</th>';
                          echo '<th>'.JText::_('COM_ALKASUBSCRIPTIONS_START_ISSUE').'</th>';
                          echo '<th>'.JText::_('COM_ALKASUBSCRIPTIONS_END_ISSUE').'</th>';
                        echo '</tr>';
                      echo '</thead>';
                      echo '<tbody>';
                      foreach($futureRenews as $renew){
                        echo '<tr>';
                        $st = new DateTime($renew->start);
                        $en = new DateTime($renew->end);
                    ?>
                        <td><span class="label label-default"><?php echo $renew->productName; ?></span></td>
                        <td><span alt="Prévu en <?php echo JText::_(strtoupper($st->format('F'))).' '.$st->format('Y'); ?>" title="Prévu en <?php echo JText::_(strtoupper($st->format('F'))).' '.$st->format('Y'); ?>"><?php echo $renew->startIssue ?></td>
                        <td><span alt="Prévu en <?php echo JText::_(strtoupper($en->format('F'))).' '.$en->format('Y'); ?>" title="Prévu en <?php echo JText::_(strtoupper($en->format('F'))).' '.$en->format('Y'); ?>"><?php echo $renew->endIssue ?></td>
                    <?php
                        echo '</tr>';
                      }
                      echo '</tbody>';
                      echo '</table></small>';
                    }
                    ?>
                </td>
                <!--<td class="center">
                  <?php
                    $type = @unserialize($subscription->productOptions);
                    if(isset($type[0]->characteristic_value)){echo $type[0]->characteristic_value;}else{echo '-';}
                  ?>
                </td>-->
                <?php /*if($subscription->paid == 1) { echo '<td>'.$subscription->startDate.'</td>'; } else { echo '<td class="center">-</td>'; } */?>
                <?php if($params->get('mode')=='duration'){ ?>
                    <?php if($subscription->paid == 1) { echo '<td>'.$subscription->endDate.'</td>'; } else { echo '<td class="center">-</td>'; }  ?>
                <?php } else if($params->get('mode')=='issue'){ ?>
                    <?php //echo '<td>'.$subscription->startIssueNumber.'</td>';?>
                    <?php //echo '<td>'.$subscription->endIssueNumber.'</td>';?>
                    <?php //echo '<td>'.$lastSent->issue.'<br/><small>Envoyé le '.$lastSentDate->format('Y-m-d').'</small>'.'</td>'; ?>
                <?php } ?>
                <td class="center" style="vertical-align:top;">
                  <?php if($subscription->paid == 1){
                      echo '<i class="fa fa-2x fa-check" aria-hidden="true"></i>';
                    } else {
                      echo '<i class="fa fa-2x fa-hourglass-half" aria-hidden="true"></i>';
                    }
                  ?>
                </td>
                <?php if($params->get('mode')=='issue' && $lastSent->issue <= $subscription->endIssueNumber && $subscription->enabled && $subscription->paid ){
                    p
                  ?>
                    <td style="vertical-align:top; text-align:center;"><button type="button" title="<?php echo JText::_('COM_ALKASUBSCRIPTIONS_EXTEND'); ?> " alt="<?php echo JText::_('COM_ALKASUBSCRIPTIONS_EXTEND'); ?> " class="btn btn-default" style="padding:7px;" onclick="extend(<?php echo $subscription->idSub; ?>,<?php echo $subscription->mainProductToRenew ? intval($subscription->mainProductToRenew) : intval($subscription->idProduct); ?>)"><i class="fa fa-calendar"></i> </button></td>
                <?php }else{ ?>
                    <td style="vertical-align:top; text-align:center;"> - </td>
                <?php } ?>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <!-- TAB 2 -->
    <div id="addresses" class="tab-pane <?php if(JFactory::getApplication()->input->get('tab',null,null)=='addresses'){echo 'in active';} ?> fade">
      <div class="row-fluid">
        <div class="span12 text-right">
          <button type="button" onclick="setAddress()" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> <?php echo JText::_('COM_ALKASUBSCRIPTIONS_ADD_ADDRESS'); ?></button>
        </div>
      </div>
      <div class="row-fluid">
        <div class="span12">
          <table class="table">
            <thead>
              <th><?php echo JText::_('COM_ALKASUBSCRIPTIONS_TITLE'); ?></th>
              <th><?php echo JText::_('COM_ALKASUBSCRIPTIONS_FIRSTNAME'); ?></th>
  						<th><?php echo JText::_('COM_ALKASUBSCRIPTIONS_LASTNAME'); ?></th>
  						<th><?php echo JText::_('COM_ALKASUBSCRIPTIONS_ADDRESS'); ?></th>
              <!-- <th><?php echo JText::_('COM_ALKASUBSCRIPTIONS_CITY'); ?></th>
  						<th><?php echo JText::_('COM_ALKASUBSCRIPTIONS_ZIP'); ?></th> -->
  						<th><?php echo JText::_('COM_ALKASUBSCRIPTIONS_COUNTRY'); ?></th>
						  <th class="center"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_ACTIONS'); ?></th>
            </thead>
            <tbody>
              <?php
                $row = 0;
                foreach ($this->addresses as $address) {
                $row++;
              ?>
              <tr>
                <td><?php echo $address->title ?></td>
                <td><?php echo $address->firstname ?></td>
                <td><?php echo $address->lastname ?></td>
                <td><?php echo $address->addressLine1 ?> <?php echo $address->addressLine2 ?><br/><?php echo $address->zip ?> <?php echo $address->city ?></td>
                <td><?php echo $address->country ?></td>
                <td>
                  <a onclick="setAddress(<?php echo $address->id ?>)" href="javascript:void(0)"><i class="fa fa-pencil-square fa-2x" aria-hidden="true"></i></a>
                  <a href="javascript:void(0)" onclick="checkLink(<?php echo $address->id ?>)"><i class="fa fa-trash fa-2x" aria-hidden="true"></i></a>
                </td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <!-- END TABS CONTENT -->
</div>

<div id="add-subscription-modal" class="modal hide fade modal-small">
  <form method="post" id="add-subscription-form" class="form-horizontal" enctype="multipart/form-data"  action="<?php echo JRoute::_('index.php'); ?>">
    <div class="modal-header">
      <a type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</a>
      <h3><?php echo JText::_('COM_ALKASUBSCRIPTIONS_ADD_SUBSCRIPTION'); ?></h3>
    </div>
    <div class="modal-body">
      <?php echo $this->formSubscription->renderFieldset('default'); ?>
    </div>
    <div class="modal-footer">
      <a class="btn" data-dismiss="modal" aria-hidden="true"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_CLOSE'); ?></a>
      <button type="submit" class="btn btn-primary"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_SUBMIT'); ?></button>
    </div>
    <input type="hidden" name="controller" value="subscriptions"/>
    <input type="hidden" name="option" value="com_alkasubscriptions"/>
    <input type="hidden" name="task"  class="task" value="add"/>
  </form>
</div>

<div id="set-address-modal" class="modal hide fade">
  <form method="post" id="set-address-form" class="form-horizontal" enctype="multipart/form-data"  action="<?php echo JRoute::_('index.php'); ?>">
    <div class="modal-header">
      <a type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</a>
      <h3><?php echo JText::_('COM_ALKASUBSCRIPTIONS_ADD_ADDRESS'); ?></h3>
    </div>
    <div class="modal-body" style="max-height:60vh !important; overflow-y:auto;">
      <?php echo $this->formAddress->renderFieldset('default'); ?>
    </div>
    <div class="modal-footer">
      <a class="btn" data-dismiss="modal" aria-hidden="true"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_CLOSE'); ?></a>
      <button type="submit" class="btn btn-primary"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_SUBMIT'); ?></button>
    </div>
    <input type="hidden" name="controller" value="addresses"/>
    <input type="hidden" name="option" value="com_alkasubscriptions"/>
    <input type="hidden" name="task"  class="task" value="setAddress"/>
  </form>
</div>

<div id="link-address-modal" class="modal hide fade" data-idsubscription="" data-idaddress="">
  <!-- <form method="post" id="link-address-form" class="form-vertical" enctype="multipart/form-data"  action="<?php echo JRoute::_('index.php'); ?>"> -->
    <div class="modal-header">
      <a type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</a>
      <h3><?php echo JText::_('COM_ALKASUBSCRIPTIONS_LINK_ADDRESS'); ?></h3>
    </div>
    <button type="button" onclick="setAddress()" class="btn btn-primary" style="margin:10px;"><i class="fa fa-plus" aria-hidden="true"></i> <?php echo JText::_('COM_ALKASUBSCRIPTIONS_ADD_ADDRESS'); ?></button>
    <div class="modal-body">
      <table class="table">
        <thead>
          <th>#</th>
          <th><?php echo JText::_('COM_ALKASUBSCRIPTIONS_TITLE'); ?></th>
          <th><?php echo JText::_('COM_ALKASUBSCRIPTIONS_FIRSTNAME'); ?></th>
          <th><?php echo JText::_('COM_ALKASUBSCRIPTIONS_LASTNAME'); ?></th>
          <th><?php echo JText::_('COM_ALKASUBSCRIPTIONS_COMPANY'); ?></th>
          <th><?php echo JText::_('COM_ALKASUBSCRIPTIONS_ADDRESS_LINE_1'); ?></th>
          <th><?php echo JText::_('COM_ALKASUBSCRIPTIONS_ADDRESS_LINE_2'); ?></th>
          <th><?php echo JText::_('COM_ALKASUBSCRIPTIONS_CITY'); ?></th>
          <th><?php echo JText::_('COM_ALKASUBSCRIPTIONS_ZIP'); ?></th>
          <th><?php echo JText::_('COM_ALKASUBSCRIPTIONS_COUNTRY'); ?></th>
          <th class="center"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_ACTIONS'); ?></th>
        </thead>
        <tbody>
          <?php
            $row = 0;
            foreach ($this->addresses as $address) {
            $row++;
          ?>
          <tr>
            <td><?php echo $row ?></td>
            <td><?php echo $address->title ?></td>
            <td><?php echo $address->firstname ?></td>
            <td><?php echo $address->lastname ?></td>
            <td><?php echo $address->company ?></td>
            <td><?php echo $address->addressLine1 ?></td>
            <td><?php echo $address->addressLine2 ?></td>
            <td><?php echo $address->city ?></td>
            <td><?php echo $address->zip ?></td>
            <td><?php echo $address->country ?></td>
            <td>
              <button id="btn-link-<?php echo $address->id ?>" class="btn btn-primary" onclick="linkAddress(<?php echo $address->id ?>)"><i class="fa fa-link" aria-hidden="true"></i> <?php echo JText::_('COM_ALKASUBSCRIPTIONS_LINK'); ?></button>
            </td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
    <div class="modal-footer">
      <a class="btn" data-dismiss="modal" aria-hidden="true"><?php echo JText::_('COM_ALKASUBSCRIPTIONS_CLOSE'); ?></a>
    </div>
    <input type="hidden" name="controller" value="addresses"/>
    <input type="hidden" name="option" value="com_alkasubscriptions"/>
    <input type="hidden" name="task"  class="task" value="link"/>
  <!-- </form> -->
</div>
